#include "include/const.h"
#include "include/sdl_utils.h"
#include "include/utils.h"

int main(int argc, char** argv){
    /* Initialisation de la SDL */
    if(-1 == SDL_Init(SDL_INIT_VIDEO))
    {
        fprintf(stderr, "Impossible d'initialiser la SDL. Fin du programme.\n");
        exit(EXIT_FAILURE);
    }

    /* Initialisation de TTF pour les textes */
    if(TTF_Init() == -1)
    {
        fprintf(stderr, "Erreur d'initialisation de TTF_Init : %s\n", TTF_GetError());
        exit(EXIT_FAILURE);
    }

    /* Ouverture d'une fenetre et creation d'un contexte OpenGL */
    SDL_Surface* surface;
    reshape(&surface, WINDOW_WIDTH, WINDOW_HEIGHT);

    /* Initialisation du titre de la fenetre */
	SDL_WM_SetCaption(WINDOW_TITLE, NULL);

	/* Gestion de la transparence des png */
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	/* D�placement du rep�re en haut � gauche */
    glTranslatef(0,WINDOW_HEIGHT,0);
    glScalef(1,-1,1);

    /*Stockage des unit�s dans un tableau*/
    Unit tabUnit[NB_UNITS];
    initUnits(tabUnit);

    SDL_Surface* tabSurfaces[NB_SURFACES];
    SDL_Surface* tabSurfacesTexts[NB_TEXTS];
    GLuint tabTextures[NB_TEXTURES];
    GLuint tabTexturesTexts[NB_TEXTS];
    TTF_Font* fontText;
    fontText = TTF_OpenFont("minecraft.ttf",60);
    initTexts(tabSurfacesTexts, tabTexturesTexts, fontText);
    loadTextures(tabTextures, tabSurfaces);

    int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW];
    createMap(tabMap, tabUnit);

    game(tabUnit, tabMap, tabTextures, tabTexturesTexts, tabSurfaces, tabSurfacesTexts, fontText);

    TTF_CloseFont(fontText);
    TTF_Quit();

    /* Liberation de la m�moire occupee par img */
    freeSurfaces(tabSurfaces, tabSurfacesTexts);
    freeTextures(tabTextures, tabTexturesTexts);
    /* Liberation des ressources associees a la SDL */
    SDL_Quit();

    return EXIT_SUCCESS;
}

