typedef struct CaseMap CaseMap;

struct CaseMap{
    int x;
    int y;
    int cost;
    int heuristic;
    int visited;
    CaseMap* next;
    CaseMap* parent;
    CaseMap* fils;
};
