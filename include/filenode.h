#include "casemap.h"

typedef struct FileNode FileNode;

struct FileNode{
    CaseMap* cell;
    FileNode* next;
};
