#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <inttypes.h>

#define NB_CASES_WIDTH_GAME 17
#define NB_CASES_WIDTH_MENU 5
#define NB_CASES_WIDTH_WINDOW 22
#define NB_CASES_HEIGHT_WINDOW 11
#define NB_TEXTURES 141
#define NB_SURFACES 141
#define NB_TEXTS 12
#define NB_UNITS 12
#define DELAY_FLICKER 200
#define DELAY_ACTION_AI 1000

/* Dimensions initiales et titre de la fenetre */
static const unsigned int WINDOW_WIDTH = 60*22; //1320
static const unsigned int WINDOW_HEIGHT = 60*11; //660
static const char WINDOW_TITLE[] = "Imac Wars 2";

/* Nombre minimal de millisecondes separant le rendu de deux images */
static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 60;

/* Nombre de bits par pixel de la fenetre */
static const unsigned int BIT_PER_PIXEL = 32;
static float aspectRatio;
