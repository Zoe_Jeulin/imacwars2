#include "file.h"

void createMap(int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW], Unit tabUnit[NB_UNITS]);

void getUnitClicked(Unit** unitSelected, Unit** enemySelected, int posClickX, int posClickY, int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW], Unit tabUnit[NB_UNITS], int textSelectCase);

int aStar(int posXUnit, int posYUnit, int index_i, int index_j, int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW]);

void loadText(char typeUnit[], char healthUnit[], char dexterityUnit[], char attackUnit[], char rangeUnit[],
                GLuint* texture_text_typeUnit, GLuint* texture_text_healthUnit, GLuint* texture_text_dexterityUnit, GLuint* texture_text_attackUnit, GLuint* texture_text_rangeUnit,
                SDL_Surface* tabSurfacesTexts[NB_TEXTS], TTF_Font* fontText);

int canPlay(int tabUnitsUsed[NB_UNITS/2]);

int canAttack(Unit unit, int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW], Unit tabUnit[NB_UNITS], Unit* tabEnemiesAttackable[6]);

int canMove(Unit unit, int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW]);

void attack(Unit* unitSelected, Unit* enemySelected, int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW]);

int attackAI(Unit* unitSelected, Unit* tabEnemiesAttackable[6], int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW]);

void move(Unit* unitSelected, int posClickX, int posClickY, int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW], int tabCasesAccessibles[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW]);

void moveAI(Unit* unitSelected, int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW], Unit tabUnit[NB_UNITS]);

void aStarAI(Unit* unitSelected, Unit* unitTarget, int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW], Unit tabUnit[NB_UNITS], int* xTargetCase, int* yTargetCase);

void clearTabUnitsUsed(int tabUnitsUsed[NB_UNITS/2], Unit tabUnit[NB_UNITS]);

void clearTabEnemies(Unit* tabEnemiesAttackable[6]);

void clearTabCases(int tabCasesAccessibles[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW]);

int checkGameOver(Unit tabUnit[NB_UNITS], int team);

void game(Unit tabUnit[NB_UNITS], int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW], GLuint tabTextures[NB_TEXTURES], GLuint tabTexturesTexts[NB_TEXTS],
           SDL_Surface* tabSurfaces[NB_SURFACES], SDL_Surface* tabSurfacesTexts[NB_TEXTS], TTF_Font* fontText);

void reinitBonus(int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW]);

void initTexts(SDL_Surface* tabSurfacesTexts[NB_TEXTS], GLuint tabTexturesTexts[NB_TEXTS], TTF_Font* fontText);

