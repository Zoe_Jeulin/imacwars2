#include "unit.h"

void reshape(SDL_Surface** surface, unsigned int width, unsigned int height);

void loadTextures(GLuint tabTextures[NB_TEXTURES], SDL_Surface* tabSurfaces[NB_SURFACES]);

void freeSurfaces(SDL_Surface* tabSurfaces[NB_SURFACES], SDL_Surface* tabSurfacesTexts[NB_TEXTS]);

void freeTextures(GLuint tabTextures[NB_TEXTURES], GLuint tabTexturesTexts[NB_TEXTS]);

void drawMap(int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW],	GLuint tabTextures[NB_TEXTURES],
             Uint32 startTime, int idUnitFlicker, int idEnemyFlicker);

void drawPlayer(int idPlayer, GLuint tabTextures[NB_TEXTURES]);

void drawUnitsLeft(Unit tabUnit[NB_UNITS], GLuint tabTextures[NB_TEXTURES]);

void drawCursor(int posMouseX, int posMouseY, GLuint tabTextures[NB_TEXTURES], Uint32 startTime);

void drawButtonAttack(GLuint tabTextures[NB_TEXTURES], Uint32 timePressed, Uint32 startTime, int buttonPressed);

void drawButtonMove(GLuint tabTextures[NB_TEXTURES], Uint32 timePressed, Uint32 startTime, int buttonPressed);

void drawButtonEnd(GLuint tabTextures[NB_TEXTURES], Uint32 timePressed, Uint32 startTime, int buttonPressed);

void drawButtonBack(GLuint tabTextures[NB_TEXTURES], Uint32 timePressed, Uint32 startTime, int buttonPressed);

void drawButtonPlay(GLuint tabTextures[NB_TEXTURES], Uint32 timePressed, Uint32 startTime, int buttonPressed);

void drawButtonQuit(GLuint tabTextures[NB_TEXTURES], Uint32 timePressed, Uint32 startTime, int buttonPressed);

void drawButtonStart(GLuint tabTextures[NB_TEXTURES], Uint32 timePressed, Uint32 startTime, int buttonPressed);

void drawButtonReplay(GLuint tabTextures[NB_TEXTURES], Uint32 timePressed, Uint32 startTime, int buttonPressed);

void drawTriangles(int posX, int posY, GLuint tabTextures[NB_TEXTURES]);

void printInfos(int posMouseX, int posMouseY, Unit tabUnitToPrint[NB_UNITS], GLuint tabTextures[NB_TEXTURES], GLuint tabTexturesTexts[NB_TEXTS], SDL_Surface* tabSurfacesTexts[NB_TEXTS], TTF_Font* fontText);

void writeSelectUnit(GLuint tabTexturesTexts[NB_TEXTS]);

void writeSelectAction(GLuint tabTexturesTexts[NB_TEXTS]);

void writeSelectEnemy(GLuint tabTexturesTexts[NB_TEXTS]);

void writeSelectCase(GLuint tabTexturesTexts[NB_TEXTS]);

void showEnemies(Unit* tabEnemiesAttackable[6], GLuint tabTextures[NB_TEXTURES], Uint32 startTime, Uint32 timePressed);

void showCases(Unit unit, int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW], int tabCasesAccessibles[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW], GLuint tabTextures[NB_TEXTURES], Uint32 startTime, Uint32 timePressed);

