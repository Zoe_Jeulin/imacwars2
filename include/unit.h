typedef struct {
	char typeUnit[10];
	int idUnit;
	int idTeam;
	int healthUnit;
	int dexterityUnit;
	int attackUnit;
	int rangeUnit;
	int posXUnit;
	int posYUnit;
	int hasAttacked;
	int hasMoved;
} Unit;

Unit* getUnitFromId(int id, Unit tabUnit[NB_UNITS]);

void setUnitUsed(int idUnit, int tabUnitsUsed[6]);

int isUsed(Unit unitSelected, int tabUnitsUsed[6]);

int isAlive(Unit unit);

void initUnits(Unit tabUnit[NB_UNITS]);

void reinitUnits(Unit tabUnit[NB_UNITS]);
