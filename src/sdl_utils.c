#include "../include/const.h"
#include "../include/sdl_utils.h"

void reshape(SDL_Surface** surface, unsigned int width, unsigned int height){
    SDL_Surface* surface_temp = SDL_SetVideoMode(width, height, BIT_PER_PIXEL, SDL_OPENGL);
    if(NULL == surface_temp){
        fprintf(stderr,"Erreur lors du redimensionnement de la fenetre.\n");
        exit(EXIT_FAILURE);
    }
    *surface = surface_temp;

    aspectRatio= width / (float) height;

    glViewport(0, 0, (*surface)->w, (*surface)->h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0,WINDOW_WIDTH,0,WINDOW_HEIGHT);
}

void loadTextures(GLuint tabTextures[NB_TEXTURES], SDL_Surface* tabSurfaces[NB_SURFACES]){

    char* tabPath[NB_SURFACES];
	/* Chargement des textures */
	//D�co
	tabPath[0] = "textures/map/sol.jpg";
	tabPath[1] = "textures/map/mur.jpg";
	tabPath[2] = "textures/map/angle_bas.jpg";
	tabPath[3] = "textures/map/angle_haut.jpg";
	tabPath[4] = "textures/map/fenetre_exterieur.jpg";
	tabPath[5] = "textures/map/fenetre_v2.jpg";
	tabPath[6] = "textures/map/fenetre_interieur.jpg";
	tabPath[7] = "textures/map/tableau_haut.jpg";
	tabPath[8] = "textures/map/tableau.jpg";
	tabPath[9] = "textures/map/tableau_bas.jpg";
    tabPath[10] = "textures/map/porte_haut.jpg";
    tabPath[11] = "textures/map/porte_bas.jpg";
    tabPath[12] = "textures/map/logo_imac.jpg";
    tabPath[13] = "textures/map/bureau_A.png";
    tabPath[14] = "textures/map/bureau_B.png";

	//Equipe 1
	tabPath[15] = "textures/units/sonia_bleu_A.png";
	tabPath[16] = "textures/units/sonia_bleu_B.png";
	tabPath[17] = "textures/units/cherrier_bleu_A.png";
	tabPath[18] = "textures/units/cherrier_bleu_B.png";
	tabPath[19] = "textures/units/evan_A.png";
	tabPath[20] = "textures/units/evan_B.png";

	//Equipe 2
	tabPath[21] = "textures/units/sonia_rouge_A.png";
	tabPath[22] = "textures/units/sonia_rouge_B.png";
	tabPath[23] = "textures/units/cherrier_rouge_A.png";
	tabPath[24] = "textures/units/cherrier_rouge_B.png";
	tabPath[25] = "textures/units/zoe_A.png";
	tabPath[26] = "textures/units/zoe_B.png";

	//Bonus
	tabPath[27] = "textures/map/feutre_A.png";
	tabPath[28] = "textures/map/feutre_B.png";
	tabPath[29] = "textures/map/biere_A.png";
	tabPath[30] = "textures/map/biere_B.png";
	tabPath[31] = "textures/map/jeudimac_A.png";
	tabPath[32] = "textures/map/jeudimac_B.png";

	//Menu
	tabPath[33] = "textures/menu/menu_01.jpg";
	tabPath[34] = "textures/menu/menu_02.jpg";
	tabPath[35] = "textures/menu/menu_03.jpg";
	tabPath[36] = "textures/menu/menu_04.jpg";
	tabPath[37] = "textures/menu/menu_05.jpg";
	tabPath[38] = "textures/menu/menu_06.jpg";
	tabPath[39] = "textures/menu/menu_07.jpg";
	tabPath[40] = "textures/menu/menu_08.jpg";
	tabPath[41] = "textures/menu/menu_09.jpg";
	tabPath[42] = "textures/menu/menu_10.jpg";
	tabPath[43] = "textures/menu/menu_11.jpg";
	tabPath[44] = "textures/menu/menu_12.jpg";
	tabPath[45] = "textures/menu/menu_13.jpg";
	tabPath[46] = "textures/menu/menu_14.jpg";
	tabPath[47] = "textures/menu/menu_15.jpg";
	tabPath[48] = "textures/menu/menu_16.jpg";
	tabPath[49] = "textures/menu/menu_17.jpg";
	tabPath[50] = "textures/menu/menu_18.jpg";
	tabPath[51] = "textures/menu/menu_19.jpg";
	tabPath[52] = "textures/menu/menu_20.jpg";
	tabPath[53] = "textures/menu/menu_21.jpg";
	tabPath[54] = "textures/menu/menu_22.jpg";
	tabPath[55] = "textures/menu/menu_23.jpg";
	tabPath[56] = "textures/menu/menu_24.jpg";
	tabPath[57] = "textures/menu/menu_25.jpg";
	tabPath[58] = "textures/menu/menu_26.jpg";
	tabPath[59] = "textures/menu/menu_27.jpg";
	tabPath[60] = "textures/menu/menu_28.jpg";
	tabPath[61] = "textures/menu/menu_29.jpg";
	tabPath[62] = "textures/menu/menu_30.jpg";
	tabPath[63] = "textures/menu/menu_31.jpg";
	tabPath[64] = "textures/menu/menu_32.jpg";
	tabPath[65] = "textures/menu/menu_33.jpg";
	tabPath[66] = "textures/menu/menu_34.jpg";
	tabPath[67] = "textures/menu/menu_35.jpg";
	tabPath[68] = "textures/menu/menu_36.jpg";
	tabPath[69] = "textures/menu/menu_37.jpg";
	tabPath[70] = "textures/menu/menu_38.jpg";
	tabPath[71] = "textures/menu/menu_39.jpg";
	tabPath[72] = "textures/menu/menu_40.jpg";
	tabPath[73] = "textures/menu/menu_41.jpg";
	tabPath[74] = "textures/menu/menu_42.jpg";
	tabPath[75] = "textures/menu/menu_43.jpg";
	tabPath[76] = "textures/menu/menu_44.jpg";
	tabPath[77] = "textures/menu/menu_45.jpg";
	tabPath[78] = "textures/menu/menu_46.jpg";
	tabPath[79] = "textures/menu/menu_47.jpg";
	tabPath[80] = "textures/menu/menu_48.jpg";
	tabPath[81] = "textures/menu/menu_49.jpg";
	tabPath[82] = "textures/menu/menu_50.jpg";
    tabPath[83] = "textures/menu/menu_51.jpg";
	tabPath[84] = "textures/menu/menu_52.jpg";
	tabPath[85] = "textures/menu/menu_53.jpg";
	tabPath[86] = "textures/menu/menu_54.jpg";
	tabPath[87] = "textures/menu/menu_55.jpg";
	tabPath[88] = "textures/menu/menu_56.jpg";
	tabPath[89] = "textures/menu/menu_57.jpg";
	tabPath[90] = "textures/menu/menu_58.jpg";
	tabPath[91] = "textures/menu/menu_59.jpg";
	tabPath[92] = "textures/menu/menu_60.jpg";
	tabPath[93] = "textures/menu/menu_61.jpg";
	tabPath[94] = "textures/buttons/attaquer_A.jpg";
	tabPath[95] = "textures/buttons/attaquer_B.jpg";
	tabPath[96] = "textures/buttons/se_deplacer_A.jpg";
	tabPath[97] = "textures/buttons/se_deplacer_B.jpg";
	tabPath[98] = "textures/buttons/finir_tour_A.jpg";
	tabPath[99] = "textures/buttons/finir_tour_B.jpg";

	// Tete des perso dans le menu
    tabPath[100] = "textures/unit_heads/cherrier_bleu.png";
    tabPath[101] = "textures/unit_heads/evan_bleu.png";
    tabPath[102] = "textures/unit_heads/sonia_bleu.png";
    tabPath[103] = "textures/unit_heads/zoe_bleu.png";

    //Bouton retour
    tabPath[104] = "textures/buttons/retour_A.jpg";
    tabPath[105] = "textures/buttons/retour_B.jpg";

    // Curseur triangles
    tabPath[106] = "textures/cursor/cursor_A.png";
    tabPath[107] = "textures/cursor/cursor_B.png";
    tabPath[108] = "textures/cursor/cursor_locked.png";

    //Cases d�placement
    tabPath[109] = "textures/deplacement/deplacement_1.png";
    tabPath[110] = "textures/deplacement/deplacement_2.png";
    tabPath[111] = "textures/deplacement/deplacement_3.png";
    tabPath[112] = "textures/deplacement/deplacement_4.png";
    tabPath[113] = "textures/deplacement/deplacement_5.png";
    tabPath[114] = "textures/deplacement/deplacement_6.png";
    tabPath[115] = "textures/deplacement/deplacement_7.png";
    tabPath[116] = "textures/deplacement/deplacement_8.png";
    tabPath[117] = "textures/deplacement/deplacement_9.png";
    tabPath[118] = "textures/deplacement/deplacement_10.png";

    //Cases attack
    tabPath[119] = "textures/attack/attack_1.png";
    tabPath[120] = "textures/attack/attack_2.png";
    tabPath[121] = "textures/attack/attack_3.png";
    tabPath[122] = "textures/attack/attack_4.png";
    tabPath[123] = "textures/attack/attack_5.png";
    tabPath[124] = "textures/attack/attack_6.png";
    tabPath[125] = "textures/attack/attack_7.png";
    tabPath[126] = "textures/attack/attack_8.png";
    tabPath[127] = "textures/attack/attack_9.png";
    tabPath[128] = "textures/attack/attack_10.png";

    //affichage : commencer � jouer, explications du jeu, fin victoire, fin d�faite
    tabPath[129] = "textures/screen/start_min.jpg";
    tabPath[130] = "textures/screen/explanation_min.jpg";
    tabPath[131] = "textures/screen/victory_min.jpg";
    tabPath[132] = "textures/screen/defeat_min.jpg";

    //Boutons des affichages
    tabPath[133] = "textures/buttons/jouer_A.png";
    tabPath[134] = "textures/buttons/jouer_B.png";

    tabPath[135] = "textures/buttons/rejouer_A.png";
    tabPath[136] = "textures/buttons/rejouer_B.png";

    tabPath[137] = "textures/buttons/quitter_A.png";
    tabPath[138] = "textures/buttons/quitter_B.png";

    tabPath[139] = "textures/buttons/commencer_A.png";
    tabPath[140] = "textures/buttons/commencer_B.png";

	//Pour chaque surface, chargement de l'image correspondante
	for(int i=0; i<NB_SURFACES; i++){
        tabSurfaces[i] = IMG_Load(tabPath[i]);
	}

	//V�rification du bon import de chaque image
    for(int i=0; i<NB_SURFACES; i++){
        if(NULL == tabSurfaces[i]) {
        fprintf(stderr, "Echec du chargement de l'image %s\n", tabPath[i]);
        exit(EXIT_FAILURE);
        }else{
            printf("Importation r�ussie : %s\n", tabPath[i]);
        }
    }

    //Pour chaque texture, affectation de la surface correspondante
    for(int i=0; i<NB_TEXTURES; i++){
        GLenum format;
        switch(tabSurfaces[i]->format->BytesPerPixel) { //D�tection du format (jpg ou png)
            case 1:
                format = GL_RED;
                break;
            case 3:
                format = GL_RGB;
                break;
            case 4:
                format = GL_RGBA;
                break;
            default:
                fprintf(stderr, "Format des pixels de l'image %s non supporte.\n", tabPath[i]);
                //return EXIT_FAILURE;
        }
        glGenTextures(1, &tabTextures[i]);
        glBindTexture(GL_TEXTURE_2D, tabTextures[i]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, format, tabSurfaces[i]->w, tabSurfaces[i]->h, 0, format, GL_UNSIGNED_BYTE, tabSurfaces[i]->pixels);
        glBindTexture(GL_TEXTURE_2D, 0);
    }


}

void freeSurfaces(SDL_Surface* tabSurfaces[NB_SURFACES], SDL_Surface* tabSurfacesTexts[NB_TEXTS]){
    for(int i=0;i<NB_SURFACES;i++){
        SDL_FreeSurface(tabSurfaces[i]);
    }
    for(int i=0;i<NB_TEXTS;i++){
        SDL_FreeSurface(tabSurfacesTexts[i]);
    }
    printf("Surfaces lib�r�es\n");
}

void freeTextures(GLuint tabTextures[NB_TEXTURES], GLuint tabTexturesTexts[NB_TEXTS]){
    for(int i=0;i<NB_TEXTURES;i++){
        glDeleteTextures(1,&tabTextures[i]);
    }
    for(int i=0;i<NB_TEXTS;i++){
        glDeleteTextures(1,&tabTexturesTexts[i]);
    }
    printf("Textures lib�r�es");
}

void drawMap(int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW],	GLuint tabTextures[NB_TEXTURES],
             Uint32 startTime, int idUnitFlicker, int idEnemyFlicker){
	int cptMenu = 0;
    int spriteAnimation = 400;

    for(int j=0;j<NB_CASES_HEIGHT_WINDOW;j++){
        for(int i=0;i<NB_CASES_WIDTH_WINDOW;i++){
            if((idUnitFlicker!=tabMap[i][j] && idEnemyFlicker!=tabMap[i][j]) || (int)floor(startTime/DELAY_FLICKER)%2==0){
                switch(tabMap[i][j]){
                    case 0: //sol
                        glBindTexture(GL_TEXTURE_2D, tabTextures[0]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 1: //mur de gauche
                        glBindTexture(GL_TEXTURE_2D, tabTextures[1]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 2: //mur du haut
                        glBindTexture(GL_TEXTURE_2D, tabTextures[1]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 3: //mur de droite
                        glBindTexture(GL_TEXTURE_2D, tabTextures[1]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 4: //angle mur bas gauche
                        glBindTexture(GL_TEXTURE_2D, tabTextures[2]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 5: //angle mur bas droite
                        glBindTexture(GL_TEXTURE_2D, tabTextures[2]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 6: //angle mur haut gauche
                        glBindTexture(GL_TEXTURE_2D, tabTextures[3]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 7: //angle mur haut droite
                        glBindTexture(GL_TEXTURE_2D, tabTextures[3]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 8: //exterieur fenetre gauche
                        glBindTexture(GL_TEXTURE_2D, tabTextures[4]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 9: //exterieur fenetre droite
                        glBindTexture(GL_TEXTURE_2D, tabTextures[4]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 10: //milieu fenetre
                        glBindTexture(GL_TEXTURE_2D, tabTextures[5]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 11: //interieur fenetre gauche
                        glBindTexture(GL_TEXTURE_2D, tabTextures[6]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 12: //interieur fenetre droite
                        glBindTexture(GL_TEXTURE_2D, tabTextures[6]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 13: //haut tableau
                        glBindTexture(GL_TEXTURE_2D, tabTextures[7]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 14: //milieu tableau
                        glBindTexture(GL_TEXTURE_2D, tabTextures[8]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 15: //bas tableau
                        glBindTexture(GL_TEXTURE_2D, tabTextures[9]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 16: //haut porte
                        glBindTexture(GL_TEXTURE_2D, tabTextures[10]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 17: //bas porte
                        glBindTexture(GL_TEXTURE_2D, tabTextures[11]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 18: //log imac
                        glBindTexture(GL_TEXTURE_2D, tabTextures[12]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        break;
                    case 19: //bureaux
                        glBindTexture(GL_TEXTURE_2D, tabTextures[0]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        if((int)floor(startTime/spriteAnimation)%2==0){
                            glBindTexture(GL_TEXTURE_2D, tabTextures[13]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }else{
                            glBindTexture(GL_TEXTURE_2D, tabTextures[14]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }
                        break;
                    case 20: //sonia bleu
                        glBindTexture(GL_TEXTURE_2D, tabTextures[0]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        if((int)floor(startTime/spriteAnimation)%2==0){
                            glBindTexture(GL_TEXTURE_2D, tabTextures[15]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }else{
                            glBindTexture(GL_TEXTURE_2D, tabTextures[16]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }
                        break;
                    case 21: //cherrier bleu 1
                        glBindTexture(GL_TEXTURE_2D, tabTextures[0]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        if((int)floor(startTime/spriteAnimation)%2==0){
                            glBindTexture(GL_TEXTURE_2D, tabTextures[17]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }else{
                            glBindTexture(GL_TEXTURE_2D, tabTextures[18]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }
                        break;
                    case 22: //cherrier bleu 2
                        glBindTexture(GL_TEXTURE_2D, tabTextures[0]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        if((int)floor(startTime/spriteAnimation)%2==0){
                            glBindTexture(GL_TEXTURE_2D, tabTextures[17]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }else{
                            glBindTexture(GL_TEXTURE_2D, tabTextures[18]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }
                        break;
                    case 23: //evan bleu 1
                        glBindTexture(GL_TEXTURE_2D, tabTextures[0]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        if((int)floor(startTime/spriteAnimation)%2==0){
                            glBindTexture(GL_TEXTURE_2D, tabTextures[19]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }else{
                            glBindTexture(GL_TEXTURE_2D, tabTextures[20]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }
                        break;
                    case 24: //evan bleu 2
                        glBindTexture(GL_TEXTURE_2D, tabTextures[0]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        if((int)floor(startTime/spriteAnimation)%2==0){
                            glBindTexture(GL_TEXTURE_2D, tabTextures[19]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }else{
                            glBindTexture(GL_TEXTURE_2D, tabTextures[20]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }
                        break;
                    case 25: //evan bleu 3
                        glBindTexture(GL_TEXTURE_2D, tabTextures[0]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        if((int)floor(startTime/spriteAnimation)%2==0){
                            glBindTexture(GL_TEXTURE_2D, tabTextures[19]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }else{
                            glBindTexture(GL_TEXTURE_2D, tabTextures[20]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }
                        break;
                    case 26: //sonia rouge
                        glBindTexture(GL_TEXTURE_2D, tabTextures[0]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        if((int)floor(startTime/spriteAnimation)%2==0){
                            glBindTexture(GL_TEXTURE_2D, tabTextures[21]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }else{
                            glBindTexture(GL_TEXTURE_2D, tabTextures[22]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }
                        break;
                    case 27: //cherrier rouge 1
                        glBindTexture(GL_TEXTURE_2D, tabTextures[0]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        if((int)floor(startTime/spriteAnimation)%2==0){
                            glBindTexture(GL_TEXTURE_2D, tabTextures[23]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }else{
                            glBindTexture(GL_TEXTURE_2D, tabTextures[24]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }
                        break;
                    case 28: //cherrier rouge 2
                        glBindTexture(GL_TEXTURE_2D, tabTextures[0]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        if((int)floor(startTime/spriteAnimation)%2==0){
                            glBindTexture(GL_TEXTURE_2D, tabTextures[23]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }else{
                            glBindTexture(GL_TEXTURE_2D, tabTextures[24]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }
                        break;
                    case 29: //zoe 1
                        glBindTexture(GL_TEXTURE_2D, tabTextures[0]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        if((int)floor(startTime/spriteAnimation)%2==0){
                            glBindTexture(GL_TEXTURE_2D, tabTextures[25]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }else{
                            glBindTexture(GL_TEXTURE_2D, tabTextures[26]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }
                        break;
                    case 30: //zoe 2
                        glBindTexture(GL_TEXTURE_2D, tabTextures[0]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        if((int)floor(startTime/spriteAnimation)%2==0){
                            glBindTexture(GL_TEXTURE_2D, tabTextures[25]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }else{
                            glBindTexture(GL_TEXTURE_2D, tabTextures[26]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }
                        break;
                    case 31: //zoe 3
                        glBindTexture(GL_TEXTURE_2D, tabTextures[0]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        if((int)floor(startTime/spriteAnimation)%2==0){
                            glBindTexture(GL_TEXTURE_2D, tabTextures[25]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }else{
                            glBindTexture(GL_TEXTURE_2D, tabTextures[26]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }
                        break;
                    case 32: //feutre
                        glBindTexture(GL_TEXTURE_2D, tabTextures[0]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        if((int)floor(startTime/spriteAnimation)%2==0){
                            glBindTexture(GL_TEXTURE_2D, tabTextures[27]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }else{
                            glBindTexture(GL_TEXTURE_2D, tabTextures[28]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }
                        break;
                    case 33: //biere
                        glBindTexture(GL_TEXTURE_2D, tabTextures[0]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        if((int)floor(startTime/spriteAnimation)%2==0){
                            glBindTexture(GL_TEXTURE_2D, tabTextures[29]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }else{
                            glBindTexture(GL_TEXTURE_2D, tabTextures[30]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }
                        break;
                    case 34: //jeudimac
                        glBindTexture(GL_TEXTURE_2D, tabTextures[0]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        if((int)floor(startTime/spriteAnimation)%2==0){
                            glBindTexture(GL_TEXTURE_2D, tabTextures[31]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }else{
                            glBindTexture(GL_TEXTURE_2D, tabTextures[32]);
                            glPushMatrix();
                            glBegin(GL_QUADS);
                                glTexCoord2f(0, 0);
                                glVertex2d(i*60,j*60);
                                glTexCoord2f(0, 1);
                                glVertex2d(i*60,j*60+60);
                                glTexCoord2f(1, 1);
                                glVertex2d(i*60+60,j*60+60);
                                glTexCoord2f(1, 0);
                                glVertex2d(i*60+60,j*60);
                            glEnd();
                            glPopMatrix();
                            glBindTexture(GL_TEXTURE_2D, 0);
                        }
                        break;
                    default: //menu
                        glBindTexture(GL_TEXTURE_2D, tabTextures[33+cptMenu]);
                        glPushMatrix();
                        glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex2d(i*60,j*60);
                            glTexCoord2f(0, 1);
                            glVertex2d(i*60,j*60+60);
                            glTexCoord2f(1, 1);
                            glVertex2d(i*60+60,j*60+60);
                            glTexCoord2f(1, 0);
                            glVertex2d(i*60+60,j*60);
                        glEnd();
                        glPopMatrix();
                        glBindTexture(GL_TEXTURE_2D, 0);
                        cptMenu++;
                        break;
                }
            }else{
                glBindTexture(GL_TEXTURE_2D, tabTextures[0]);
                glPushMatrix();
                glBegin(GL_QUADS);
                    glTexCoord2f(0, 0);
                    glVertex2d(i*60,j*60);
                    glTexCoord2f(0, 1);
                    glVertex2d(i*60,j*60+60);
                    glTexCoord2f(1, 1);
                    glVertex2d(i*60+60,j*60+60);
                    glTexCoord2f(1, 0);
                    glVertex2d(i*60+60,j*60);
                glEnd();
                glPopMatrix();
                glBindTexture(GL_TEXTURE_2D, 0);
            }
        }
    }
}

void drawPlayer(int idPlayer, GLuint tabTextures[NB_TEXTURES]){
    if(idPlayer==1){
        glBindTexture(GL_TEXTURE_2D, tabTextures[41]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(1200,60);
            glTexCoord2f(0, 1);
            glVertex2d(1200,120);
            glTexCoord2f(1, 1);
            glVertex2d(1260,120);
            glTexCoord2f(1, 0);
            glVertex2d(1260,60);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);
    }else{
        glBindTexture(GL_TEXTURE_2D, tabTextures[88]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(1200,60);
            glTexCoord2f(0, 1);
            glVertex2d(1200,120);
            glTexCoord2f(1, 1);
            glVertex2d(1260,120);
            glTexCoord2f(1, 0);
            glVertex2d(1260,60);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

void drawUnitsLeft(Unit tabUnit[NB_UNITS], GLuint tabTextures[NB_TEXTURES]){
    int nbUnitsLeft = 0;

    for(int i=0; i<NB_UNITS; i++){
        if(tabUnit[i].idTeam==1 && tabUnit[i].healthUnit>0){
            nbUnitsLeft++;
        }
    }
    switch(nbUnitsLeft){
        case 1:
            glBindTexture(GL_TEXTURE_2D, tabTextures[58]);
            glPushMatrix();
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2d(17*60,5*60);
                glTexCoord2f(0, 1);
                glVertex2d(17*60,6*60);
                glTexCoord2f(1, 1);
                glVertex2d(18*60,6*60);
                glTexCoord2f(1, 0);
                glVertex2d(18*60,5*60);
            glEnd();
            glPopMatrix();
            glBindTexture(GL_TEXTURE_2D, 0);
            break;
        case 2:
            glBindTexture(GL_TEXTURE_2D, tabTextures[89]);
            glPushMatrix();
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2d(17*60,5*60);
                glTexCoord2f(0, 1);
                glVertex2d(17*60,6*60);
                glTexCoord2f(1, 1);
                glVertex2d(18*60,6*60);
                glTexCoord2f(1, 0);
                glVertex2d(18*60,5*60);
            glEnd();
            glPopMatrix();
            glBindTexture(GL_TEXTURE_2D, 0);
            break;
        case 3:
            glBindTexture(GL_TEXTURE_2D, tabTextures[90]);
            glPushMatrix();
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2d(17*60,5*60);
                glTexCoord2f(0, 1);
                glVertex2d(17*60,6*60);
                glTexCoord2f(1, 1);
                glVertex2d(18*60,6*60);
                glTexCoord2f(1, 0);
                glVertex2d(18*60,5*60);
            glEnd();
            glPopMatrix();
            glBindTexture(GL_TEXTURE_2D, 0);
            break;
        case 4:
            glBindTexture(GL_TEXTURE_2D, tabTextures[91]);
            glPushMatrix();
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2d(17*60,5*60);
                glTexCoord2f(0, 1);
                glVertex2d(17*60,6*60);
                glTexCoord2f(1, 1);
                glVertex2d(18*60,6*60);
                glTexCoord2f(1, 0);
                glVertex2d(18*60,5*60);
            glEnd();
            glPopMatrix();
            glBindTexture(GL_TEXTURE_2D, 0);
            break;
        case 5:
            glBindTexture(GL_TEXTURE_2D, tabTextures[92]);
            glPushMatrix();
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2d(17*60,5*60);
                glTexCoord2f(0, 1);
                glVertex2d(17*60,6*60);
                glTexCoord2f(1, 1);
                glVertex2d(18*60,6*60);
                glTexCoord2f(1, 0);
                glVertex2d(18*60,5*60);
            glEnd();
            glPopMatrix();
            glBindTexture(GL_TEXTURE_2D, 0);
            break;
        case 6:
            glBindTexture(GL_TEXTURE_2D, tabTextures[93]);
            glPushMatrix();
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2d(17*60,5*60);
                glTexCoord2f(0, 1);
                glVertex2d(17*60,6*60);
                glTexCoord2f(1, 1);
                glVertex2d(18*60,6*60);
                glTexCoord2f(1, 0);
                glVertex2d(18*60,5*60);
            glEnd();
            glPopMatrix();
            glBindTexture(GL_TEXTURE_2D, 0);
            break;
    }
}

void drawCursor(int posMouseX, int posMouseY, GLuint tabTextures[NB_TEXTURES], Uint32 startTime){
    int i = posMouseX/60;
    int j = posMouseY/60;
    if(i>0 && i<16 && j>0){ //si le curseur est dans l'aire de jeu
        if((int)floor(startTime/400)%2==0){
            glBindTexture(GL_TEXTURE_2D, tabTextures[106]);
            glPushMatrix();
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2d(i*60,j*60);
                glTexCoord2f(0, 1);
                glVertex2d(i*60,j*60+60);
                glTexCoord2f(1, 1);
                glVertex2d(i*60+60,j*60+60);
                glTexCoord2f(1, 0);
                glVertex2d(i*60+60,j*60);
            glEnd();
            glPopMatrix();
            glBindTexture(GL_TEXTURE_2D, 0);
        }else{
            glBindTexture(GL_TEXTURE_2D, tabTextures[107]);
            glPushMatrix();
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2d(i*60,j*60);
                glTexCoord2f(0, 1);
                glVertex2d(i*60,j*60+60);
                glTexCoord2f(1, 1);
                glVertex2d(i*60+60,j*60+60);
                glTexCoord2f(1, 0);
                glVertex2d(i*60+60,j*60);
            glEnd();
            glPopMatrix();
            glBindTexture(GL_TEXTURE_2D, 0);
        }
    }
}

void drawButtonAttack(GLuint tabTextures[NB_TEXTURES], Uint32 timePressed, Uint32 startTime, int buttonPressed){
    if((startTime/200)-(timePressed/200)<1 && buttonPressed==1){
        glBindTexture(GL_TEXTURE_2D, tabTextures[95]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(1080, 430);
            glTexCoord2f(0, 1);
            glVertex2d(1080, 469);
            glTexCoord2f(1, 1);
            glVertex2d(1263, 469);
            glTexCoord2f(1, 0);
            glVertex2d(1263, 430);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);
    }else{
        glBindTexture(GL_TEXTURE_2D, tabTextures[94]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(1080, 430);
            glTexCoord2f(0, 1);
            glVertex2d(1080, 469);
            glTexCoord2f(1, 1);
            glVertex2d(1263, 469);
            glTexCoord2f(1, 0);
            glVertex2d(1263, 430);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

void drawButtonMove(GLuint tabTextures[NB_TEXTURES], Uint32 timePressed, Uint32 startTime, int buttonPressed){
    if((startTime/200)-(timePressed/200)<1 && buttonPressed==2){
        glBindTexture(GL_TEXTURE_2D, tabTextures[97]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(1080, 495);
            glTexCoord2f(0, 1);
            glVertex2d(1080, 534);
            glTexCoord2f(1, 1);
            glVertex2d(1263, 534);
            glTexCoord2f(1, 0);
            glVertex2d(1263, 495);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);
    }else{
        glBindTexture(GL_TEXTURE_2D, tabTextures[96]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(1080, 495);
            glTexCoord2f(0, 1);
            glVertex2d(1080, 534);
            glTexCoord2f(1, 1);
            glVertex2d(1263, 534);
            glTexCoord2f(1, 0);
            glVertex2d(1263, 495);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

void drawButtonEnd(GLuint tabTextures[NB_TEXTURES], Uint32 timePressed, Uint32 startTime, int buttonPressed){
    if((startTime/200)-(timePressed/200)<1 && buttonPressed==3){
        glBindTexture(GL_TEXTURE_2D, tabTextures[99]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(1080, 560);
            glTexCoord2f(0, 1);
            glVertex2d(1080, 599);
            glTexCoord2f(1, 1);
            glVertex2d(1263, 599);
            glTexCoord2f(1, 0);
            glVertex2d(1263, 560);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);
    }else{
        glBindTexture(GL_TEXTURE_2D, tabTextures[98]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(1080, 560);
            glTexCoord2f(0, 1);
            glVertex2d(1080, 599);
            glTexCoord2f(1, 1);
            glVertex2d(1263, 599);
            glTexCoord2f(1, 0);
            glVertex2d(1263, 560);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

void drawButtonBack(GLuint tabTextures[NB_TEXTURES], Uint32 timePressed, Uint32 startTime, int buttonPressed){
    if((startTime/200)-(timePressed/200)<1 && buttonPressed==4){
        glBindTexture(GL_TEXTURE_2D, tabTextures[105]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(1080, 560);
            glTexCoord2f(0, 1);
            glVertex2d(1080, 599);
            glTexCoord2f(1, 1);
            glVertex2d(1263, 599);
            glTexCoord2f(1, 0);
            glVertex2d(1263, 560);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);
    }else{
        glBindTexture(GL_TEXTURE_2D, tabTextures[104]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(1080, 560);
            glTexCoord2f(0, 1);
            glVertex2d(1080, 599);
            glTexCoord2f(1, 1);
            glVertex2d(1263, 599);
            glTexCoord2f(1, 0);
            glVertex2d(1263, 560);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

void drawButtonPlay(GLuint tabTextures[NB_TEXTURES], Uint32 timePressed, Uint32 startTime, int buttonPressed){
    if((startTime/200)-(timePressed/200)<1 && buttonPressed==5){
        glBindTexture(GL_TEXTURE_2D, tabTextures[134]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(457, 545);
            glTexCoord2f(0, 1);
            glVertex2d(457, 584);
            glTexCoord2f(1, 1);
            glVertex2d(642, 584);
            glTexCoord2f(1, 0);
            glVertex2d(642, 545);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);
    }else{
        glBindTexture(GL_TEXTURE_2D, tabTextures[133]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(457, 545);
            glTexCoord2f(0, 1);
            glVertex2d(457, 584);
            glTexCoord2f(1, 1);
            glVertex2d(642, 584);
            glTexCoord2f(1, 0);
            glVertex2d(642, 545);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

void drawButtonQuit(GLuint tabTextures[NB_TEXTURES], Uint32 timePressed, Uint32 startTime, int buttonPressed){
    if((startTime/200)-(timePressed/200)<1 && buttonPressed==6){
        glBindTexture(GL_TEXTURE_2D, tabTextures[138]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(682, 545);
            glTexCoord2f(0, 1);
            glVertex2d(682, 584);
            glTexCoord2f(1, 1);
            glVertex2d(867, 584);
            glTexCoord2f(1, 0);
            glVertex2d(867, 545);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);
    }else{
        glBindTexture(GL_TEXTURE_2D, tabTextures[137]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(682, 545);
            glTexCoord2f(0, 1);
            glVertex2d(682, 584);
            glTexCoord2f(1, 1);
            glVertex2d(867, 584);
            glTexCoord2f(1, 0);
            glVertex2d(867, 545);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

void drawButtonStart(GLuint tabTextures[NB_TEXTURES], Uint32 timePressed, Uint32 startTime, int buttonPressed){
    if((startTime/200)-(timePressed/200)<1 && buttonPressed==7){
        glBindTexture(GL_TEXTURE_2D, tabTextures[140]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(1106, 592);
            glTexCoord2f(0, 1);
            glVertex2d(1106, 631);
            glTexCoord2f(1, 1);
            glVertex2d(1289, 631);
            glTexCoord2f(1, 0);
            glVertex2d(1289, 592);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);
    }else{
        glBindTexture(GL_TEXTURE_2D, tabTextures[139]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(1106, 592);
            glTexCoord2f(0, 1);
            glVertex2d(1106, 631);
            glTexCoord2f(1, 1);
            glVertex2d(1289, 631);
            glTexCoord2f(1, 0);
            glVertex2d(1289, 592);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

void drawButtonReplay(GLuint tabTextures[NB_TEXTURES], Uint32 timePressed, Uint32 startTime, int buttonPressed){
    if((startTime/200)-(timePressed/200)<1 && buttonPressed==8){
        glBindTexture(GL_TEXTURE_2D, tabTextures[136]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(457, 545);
            glTexCoord2f(0, 1);
            glVertex2d(457, 584);
            glTexCoord2f(1, 1);
            glVertex2d(642, 584);
            glTexCoord2f(1, 0);
            glVertex2d(642, 545);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);
    }else{
        glBindTexture(GL_TEXTURE_2D, tabTextures[135]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(457, 545);
            glTexCoord2f(0, 1);
            glVertex2d(457, 584);
            glTexCoord2f(1, 1);
            glVertex2d(642, 584);
            glTexCoord2f(1, 0);
            glVertex2d(642, 545);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

void drawTriangles(int posX, int posY, GLuint tabTextures[NB_TEXTURES]){
    glBindTexture(GL_TEXTURE_2D, tabTextures[108]);
    glPushMatrix();
    glBegin(GL_QUADS);
        glTexCoord2f(0, 0);
        glVertex2d(posX*60,posY*60);
        glTexCoord2f(0, 1);
        glVertex2d(posX*60,posY*60+60);
        glTexCoord2f(1, 1);
        glVertex2d(posX*60+60,posY*60+60);
        glTexCoord2f(1, 0);
        glVertex2d(posX*60+60,posY*60);
    glEnd();
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);
}

void printInfos(int posMouseX, int posMouseY, Unit tabUnit[NB_UNITS], GLuint tabTextures[NB_TEXTURES], GLuint tabTexturesTexts[NB_TEXTS], SDL_Surface* tabSurfacesTexts[NB_TEXTS], TTF_Font* fontText){
    int i = posMouseX/60;
    int j = posMouseY/60;
    char stringHealth[4];
    char stringDexterity[3];
    char stringAttack[3];
    char stringRange[2];
    char health[10]="";
    char dexterity[10]="DEX : ";
    char attack[10]="ATT : ";
    char range[10]="POR : ";

    for(int iUnit=0; iUnit<NB_UNITS; iUnit++){
        if(tabUnit[iUnit].healthUnit>0 && tabUnit[iUnit].posXUnit==i && tabUnit[iUnit].posYUnit==j){
            //Conversion des int en string
            sprintf(stringHealth, "%d", tabUnit[iUnit].healthUnit);
            strcat(health, stringHealth);

            sprintf(stringDexterity, "%d", tabUnit[iUnit].dexterityUnit);
            strcat(dexterity, stringDexterity);

            sprintf(stringAttack, "%d", tabUnit[iUnit].attackUnit);
            strcat(attack, stringAttack);

            sprintf(stringRange, "%d", tabUnit[iUnit].rangeUnit);
            strcat(range, stringRange);

            loadText(tabUnit[iUnit].typeUnit, health, dexterity, attack, range,
                     &tabTexturesTexts[7], &tabTexturesTexts[8], &tabTexturesTexts[9], &tabTexturesTexts[10], &tabTexturesTexts[11], tabSurfacesTexts, fontText);

            //Calcul nombre de caracteres de chaque string pour affichage taille proportionnelle
            int cptLetterType=0;
            int cptLetterHealth=0;
            int cptLetterDexterity=0;
            int cptLetterAttack=0;
            int cptLetterRange=0;

            for(int o=0; tabUnit[iUnit].typeUnit[o]!='\0'; o++){
                cptLetterType++;
            }
            for(int o=0; health[o]!='\0'; o++){
                cptLetterHealth++;
            }
            for(int o=0; dexterity[o]!='\0'; o++){
                cptLetterDexterity++;
            }
            for(int o=0; attack[o]!='\0'; o++){
                cptLetterAttack++;
            }
            for(int o=0; range[o]!='\0'; o++){
                cptLetterRange++;
            }

            //Affichage type unite
            glBindTexture(GL_TEXTURE_2D, tabTexturesTexts[7]);
            glPushMatrix();
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2d(6*(10-cptLetterType)+1100,260);
                glTexCoord2f(0, 1);
                glVertex2d(6*(10-cptLetterType)+1100,295);
                glTexCoord2f(1, 1);
                glVertex2d(6*(10-cptLetterType)+1100+15*cptLetterType,295);
                glTexCoord2f(1, 0);
                glVertex2d(6*(10-cptLetterType)+1100+15*cptLetterType,260);
            glEnd();
            glPopMatrix();
            glBindTexture(GL_TEXTURE_2D, 0);

            //Affichage vie unite
            glBindTexture(GL_TEXTURE_2D, tabTexturesTexts[11]);
            glPushMatrix();
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2d(1165,143);
                glTexCoord2f(0, 1);
                glVertex2d(1165,163);
                glTexCoord2f(1, 1);
                glVertex2d(1165+10*cptLetterRange,163);
                glTexCoord2f(1, 0);
                glVertex2d(1165+10*cptLetterRange,143);
            glEnd();
            glPopMatrix();
            glBindTexture(GL_TEXTURE_2D, 0);

            //affichage barre de vie
            glBegin(GL_QUADS);
            glColor4f(255.,0.,0.,1.);
            glVertex2d(1117,235);
            glVertex2d(1117,245);
            if(tabUnit[iUnit].healthUnit>100){
                glVertex2d(1217,245);
                glVertex2d(1217,235);
            }else{
                glVertex2d(1117+tabUnit[iUnit].healthUnit,245);
                glVertex2d(1117+tabUnit[iUnit].healthUnit,235);
            }
            glEnd();
            glColor3ub(255,255,255);

            glBegin(GL_LINES);
            glColor4f(1.,33.,67.,1.);
            glVertex2d(1117,235);
            glVertex2d(1117,245);

            glVertex2d(1117,245);
            glVertex2d(1217,245);

            glVertex2d(1217,245);
            glVertex2d(1217,235);

            glVertex2d(1217,235);
            glVertex2d(1117,235);
            glEnd();
            glColor3ub(255,255,255);

            glBindTexture(GL_TEXTURE_2D, tabTexturesTexts[8]);
            glPushMatrix();
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2d(1154,232);
                glTexCoord2f(0, 1);
                glVertex2d(1154,252);
                glTexCoord2f(1, 1);
                glVertex2d(1153+12*cptLetterHealth,252);
                glTexCoord2f(1, 0);
                glVertex2d(1153+12*cptLetterHealth,232);
            glEnd();
            glPopMatrix();
            glBindTexture(GL_TEXTURE_2D, 0);

            //Affichage dexterite unite
            glBindTexture(GL_TEXTURE_2D, tabTexturesTexts[9]);
            glPushMatrix();
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2d(1165,173);
                glTexCoord2f(0, 1);
                glVertex2d(1165,193);
                glTexCoord2f(1, 1);
                glVertex2d(1165+10*cptLetterDexterity,193);
                glTexCoord2f(1, 0);
                glVertex2d(1165+10*cptLetterDexterity,173);
            glEnd();
            glPopMatrix();
            glBindTexture(GL_TEXTURE_2D, 0);

            //Affichage attaque unite
            glBindTexture(GL_TEXTURE_2D, tabTexturesTexts[10]);
            glPushMatrix();
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2d(1165,203);
                glTexCoord2f(0, 1);
                glVertex2d(1165,223);
                glTexCoord2f(1, 1);
                glVertex2d(1165+10*cptLetterAttack,223);
                glTexCoord2f(1, 0);
                glVertex2d(1165+10*cptLetterAttack,203);
            glEnd();
            glPopMatrix();
            glBindTexture(GL_TEXTURE_2D, 0);

            if(strcmp(tabUnit[iUnit].typeUnit, "Cherrier") == 0){
                glBindTexture(GL_TEXTURE_2D, tabTextures[100]);
                glPushMatrix();
                glBegin(GL_QUADS);
                    glTexCoord2f(0, 0);
                    glVertex2d(1082,140);
                    glTexCoord2f(0, 1);
                    glVertex2d(1082,220);
                    glTexCoord2f(1, 1);
                    glVertex2d(1162,220);
                    glTexCoord2f(1, 0);
                    glVertex2d(1162,140);
                glEnd();
                glPopMatrix();
                glBindTexture(GL_TEXTURE_2D, 0);
            }else if(strcmp(tabUnit[iUnit].typeUnit, "Evan") == 0){
                glBindTexture(GL_TEXTURE_2D, tabTextures[101]);
                glPushMatrix();
                glBegin(GL_QUADS);
                    glTexCoord2f(0, 0);
                    glVertex2d(1082,140);
                    glTexCoord2f(0, 1);
                    glVertex2d(1082,220);
                    glTexCoord2f(1, 1);
                    glVertex2d(1162,220);
                    glTexCoord2f(1, 0);
                    glVertex2d(1162,140);
                glEnd();
                glPopMatrix();
                glBindTexture(GL_TEXTURE_2D, 0);
            }else if(strcmp(tabUnit[iUnit].typeUnit, "Sonia") == 0){
                glBindTexture(GL_TEXTURE_2D, tabTextures[102]);
                glPushMatrix();
                glBegin(GL_QUADS);
                    glTexCoord2f(0, 0);
                    glVertex2d(1082,140);
                    glTexCoord2f(0, 1);
                    glVertex2d(1082,220);
                    glTexCoord2f(1, 1);
                    glVertex2d(1162,220);
                    glTexCoord2f(1, 0);
                    glVertex2d(1162,140);
                glEnd();
                glPopMatrix();
                glBindTexture(GL_TEXTURE_2D, 0);
            }else{
                glBindTexture(GL_TEXTURE_2D, tabTextures[103]);
                glPushMatrix();
                glBegin(GL_QUADS);
                    glTexCoord2f(0, 0);
                    glVertex2d(1082,140);
                    glTexCoord2f(0, 1);
                    glVertex2d(1082,220);
                    glTexCoord2f(1, 1);
                    glVertex2d(1162,220);
                    glTexCoord2f(1, 0);
                    glVertex2d(1162,140);
                glEnd();
                glPopMatrix();
                glBindTexture(GL_TEXTURE_2D, 0);
            }
        }
    }

    for(int i=7; i<NB_TEXTS; i++){
        glDeleteTextures(1,&tabTexturesTexts[i]);
    }
}

void writeSelectUnit(GLuint tabTexturesTexts[NB_TEXTS]){
    glBindTexture(GL_TEXTURE_2D, tabTexturesTexts[0]);
    glPushMatrix();
    glBegin(GL_QUADS);
        glTexCoord2f(0, 0);
        glVertex2d(1090, 370);
        glTexCoord2f(0, 1);
        glVertex2d(1090, 390);
        glTexCoord2f(1, 1);
        glVertex2d(1240, 390);
        glTexCoord2f(1, 0);
        glVertex2d(1240, 370);
    glEnd();
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    glBindTexture(GL_TEXTURE_2D, tabTexturesTexts[1]);
    glPushMatrix();
    glBegin(GL_QUADS);
        glTexCoord2f(0, 0);
        glVertex2d(1075, 395);
        glTexCoord2f(0, 1);
        glVertex2d(1075, 415);
        glTexCoord2f(1, 1);
        glVertex2d(1258, 415);
        glTexCoord2f(1, 0);
        glVertex2d(1258, 395);
    glEnd();
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);
}

void writeSelectAction(GLuint tabTexturesTexts[NB_TEXTS]){
    glBindTexture(GL_TEXTURE_2D, tabTexturesTexts[6]);
    glPushMatrix();
    glBegin(GL_QUADS);
        glTexCoord2f(0, 0);
        glVertex2d(1104, 395);
        glTexCoord2f(0, 1);
        glVertex2d(1104, 415);
        glTexCoord2f(1, 1);
        glVertex2d(1232, 415);
        glTexCoord2f(1, 0);
        glVertex2d(1232, 395);
    glEnd();
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);
}

void writeSelectEnemy(GLuint tabTexturesTexts[NB_TEXTS]){
    glBindTexture(GL_TEXTURE_2D, tabTexturesTexts[2]);
    glPushMatrix();
    glBegin(GL_QUADS);
        glTexCoord2f(0, 0);
        glVertex2d(1104, 370);
        glTexCoord2f(0, 1);
        glVertex2d(1104, 390);
        glTexCoord2f(1, 1);
        glVertex2d(1232, 390);
        glTexCoord2f(1, 0);
        glVertex2d(1232, 370);
    glEnd();
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    glBindTexture(GL_TEXTURE_2D, tabTexturesTexts[3]);
    glPushMatrix();
    glBegin(GL_QUADS);
        glTexCoord2f(0, 0);
        glVertex2d(1096, 395);
        glTexCoord2f(0, 1);
        glVertex2d(1096, 415);
        glTexCoord2f(1, 1);
        glVertex2d(1241, 415);
        glTexCoord2f(1, 0);
        glVertex2d(1241, 395);
    glEnd();
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);
}

void writeSelectCase(GLuint tabTexturesTexts[NB_TEXTS]){
    glBindTexture(GL_TEXTURE_2D, tabTexturesTexts[4]);
    glPushMatrix();
    glBegin(GL_QUADS);
        glTexCoord2f(0, 0);
        glVertex2d(1104, 370);
        glTexCoord2f(0, 1);
        glVertex2d(1104, 390);
        glTexCoord2f(1, 1);
        glVertex2d(1232, 390);
        glTexCoord2f(1, 0);
        glVertex2d(1232, 370);
    glEnd();
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);

    glBindTexture(GL_TEXTURE_2D, tabTexturesTexts[5]);
    glPushMatrix();
    glBegin(GL_QUADS);
        glTexCoord2f(0, 0);
        glVertex2d(1080, 395);
        glTexCoord2f(0, 1);
        glVertex2d(1080, 415);
        glTexCoord2f(1, 1);
        glVertex2d(1255, 415);
        glTexCoord2f(1, 0);
        glVertex2d(1255, 395);
    glEnd();
    glPopMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);
}

void showEnemies(Unit* tabEnemiesAttackable[6], GLuint tabTextures[NB_TEXTURES], Uint32 startTime, Uint32 timePressed){
    for(int i=0; i<6; i++){
        if(tabEnemiesAttackable[i]!=NULL){
            if(tabEnemiesAttackable[i]->idUnit!=0){
                if((int)floor(startTime-timePressed) >=0 && (int)floor(startTime-timePressed) < 100){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[119]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }else if((int)floor(startTime-timePressed) >=100 && (int)floor(startTime-timePressed) < 200){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[120]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }else if((int)floor(startTime-timePressed) >= 200 && (int)floor(startTime-timePressed) < 300){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[121]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }else if((int)floor(startTime-timePressed) >= 300 && (int)floor(startTime-timePressed) < 400){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[122]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }else if((int)floor(startTime-timePressed) >= 400 && (int)floor(startTime-timePressed) < 500){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[123]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }else if((int)floor(startTime-timePressed) >= 500 && (int)floor(startTime-timePressed) < 600){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[124]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }else if((int)floor(startTime-timePressed) >= 600 && (int)floor(startTime-timePressed) < 700){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[125]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }else if((int)floor(startTime-timePressed) >= 700 && (int)floor(startTime-timePressed) < 800){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[126]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }else if((int)floor(startTime-timePressed) >= 800 && (int)floor(startTime-timePressed) < 900){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[127]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }else if((int)floor(startTime-timePressed) >= 900 && (int)floor(startTime-timePressed) < 1000){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[128]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(tabEnemiesAttackable[i]->posXUnit*60+60,tabEnemiesAttackable[i]->posYUnit*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }
            }
        }
    }
}

void showCases(Unit unit, int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW], int tabCasesAccessibles[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW], GLuint tabTextures[NB_TEXTURES], Uint32 startTime, Uint32 timePressed){
    int distance;
    //pour chaque case dans le carr� r�duit autour de l'unit�
	 for(int i = unit.posXUnit-4 ; i <= unit.posXUnit+4 ; i++){
        if(i>=0 && i<=17){
            for(int j = unit.posYUnit-4 ; j <= unit.posYUnit+4 ; j++){
                if(j>=0 && j<=11){
                    if(tabMap[i][j]==0 || tabMap[i][j]==32 || tabMap[i][j]==33 || tabMap[i][j]==34){
                        distance=aStar(unit.posXUnit, unit.posYUnit, i, j, tabMap); //on calcule la distance entre l'unit� et cette case
                        if(distance<=unit.dexterityUnit){
                            tabCasesAccessibles[i][j]=1;
                        }
                    }
                }
            }
        }
    }

    for(int j=0;j<NB_CASES_HEIGHT_WINDOW;j++){ //modif
        for(int i=0;i<NB_CASES_WIDTH_WINDOW;i++){
            if(tabCasesAccessibles[i][j]==1){
                if((int)floor(startTime-timePressed) >=0 && (int)floor(startTime-timePressed) < 100){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[109]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(i*60,j*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(i*60,j*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(i*60+60,j*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(i*60+60,j*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }else if((int)floor(startTime-timePressed) >=100 && (int)floor(startTime-timePressed) < 200){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[110]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(i*60,j*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(i*60,j*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(i*60+60,j*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(i*60+60,j*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }else if((int)floor(startTime-timePressed) >= 200 && (int)floor(startTime-timePressed) < 300){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[111]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(i*60,j*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(i*60,j*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(i*60+60,j*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(i*60+60,j*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }else if((int)floor(startTime-timePressed) >= 300 && (int)floor(startTime-timePressed) < 400){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[112]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(i*60,j*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(i*60,j*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(i*60+60,j*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(i*60+60,j*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }else if((int)floor(startTime-timePressed) >= 400 && (int)floor(startTime-timePressed) < 500){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[113]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(i*60,j*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(i*60,j*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(i*60+60,j*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(i*60+60,j*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }else if((int)floor(startTime-timePressed) >= 500 && (int)floor(startTime-timePressed) < 600){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[114]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(i*60,j*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(i*60,j*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(i*60+60,j*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(i*60+60,j*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }else if((int)floor(startTime-timePressed) >= 600 && (int)floor(startTime-timePressed) < 700){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[115]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(i*60,j*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(i*60,j*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(i*60+60,j*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(i*60+60,j*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }else if((int)floor(startTime-timePressed) >= 700 && (int)floor(startTime-timePressed) < 800){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[116]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(i*60,j*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(i*60,j*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(i*60+60,j*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(i*60+60,j*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }else if((int)floor(startTime-timePressed) >= 800 && (int)floor(startTime-timePressed) < 900){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[117]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(i*60,j*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(i*60,j*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(i*60+60,j*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(i*60+60,j*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }else if((int)floor(startTime-timePressed) >= 900 && (int)floor(startTime-timePressed) < 1000){
                    glBindTexture(GL_TEXTURE_2D, tabTextures[118]);
                    glPushMatrix();
                    glBegin(GL_QUADS);
                        glTexCoord2f(0, 0);
                        glVertex2d(i*60,j*60);
                        glTexCoord2f(0, 1);
                        glVertex2d(i*60,j*60+60);
                        glTexCoord2f(1, 1);
                        glVertex2d(i*60+60,j*60+60);
                        glTexCoord2f(1, 0);
                        glVertex2d(i*60+60,j*60);
                    glEnd();
                    glPopMatrix();
                    glBindTexture(GL_TEXTURE_2D, 0);
                }
            }
        }
    }
}

