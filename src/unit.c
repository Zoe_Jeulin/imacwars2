#include "../include/const.h"
#include "../include/unit.h"

Unit* getUnitFromId(int id, Unit tabUnit[NB_UNITS]){
    for(int i=0; i<NB_UNITS; i++){
        if(tabUnit[i].idUnit==id){
            return &tabUnit[i];
        }
    }
    return NULL;
}

void setUnitUsed(int idUnit, int tabUnitsUsed[6]){
    int i=0;
    while(tabUnitsUsed[i]!=0){
        i++;
    }
    tabUnitsUsed[i]=idUnit;
}

int isUsed(Unit unitSelected, int tabUnitsUsed[6]){
    for(int i=0; i<6; i++){
        if(tabUnitsUsed[i]==unitSelected.idUnit){
            return 1;
        }
    }
    return 0;
}

int isAlive(Unit unit){
    if(unit.healthUnit>0){
        return 1;
    }else{
        return 0;
    }
}

void initUnits(Unit tabUnit[NB_UNITS]){
    /* D�claration des unit�s*/
    //Equipe bleue
    Unit sonia_team1;
    Unit cherrier_1_team1;
    Unit cherrier_2_team1;
    Unit evan_1;
    Unit evan_2;
    Unit evan_3;

    //Equipe rouge
    Unit sonia_team2;
    Unit cherrier_1_team2;
    Unit cherrier_2_team2;
    Unit zoe_1;
    Unit zoe_2;
    Unit zoe_3;

    /* Equipe bleue */
	// Sonia
	sonia_team1.idTeam = 1;
	strcpy(sonia_team1.typeUnit,"Sonia");
	sonia_team1.idUnit = 20;
	sonia_team1.healthUnit = 100;
	sonia_team1.dexterityUnit = 2;
	sonia_team1.attackUnit = 15;
	sonia_team1.rangeUnit = 4;
	sonia_team1.posXUnit = 1; //remettre � 1
	sonia_team1.posYUnit = 5;
	sonia_team1.hasAttacked = 0;
	sonia_team1.hasMoved = 0;

	// Cherrier
	cherrier_1_team1.idTeam = 1;
	strcpy(cherrier_1_team1.typeUnit,"Cherrier");
	cherrier_1_team1.idUnit = 21;
	cherrier_1_team1.healthUnit = 100;
	cherrier_1_team1.dexterityUnit = 3;
	cherrier_1_team1.attackUnit = 20;
	cherrier_1_team1.rangeUnit = 2;
	cherrier_1_team1.posXUnit = 2;
	cherrier_1_team1.posYUnit = 3;
	cherrier_1_team1.hasAttacked = 0;
	cherrier_1_team1.hasMoved = 0;

	cherrier_2_team1.idTeam = 1;
	strcpy(cherrier_2_team1.typeUnit,"Cherrier");
	cherrier_2_team1.idUnit = 22;
	cherrier_2_team1.healthUnit = 100;
	cherrier_2_team1.dexterityUnit = 3;
	cherrier_2_team1.attackUnit = 20;
	cherrier_2_team1.rangeUnit = 2;
	cherrier_2_team1.posXUnit = 2;
	cherrier_2_team1.posYUnit = 7;
	cherrier_2_team1.hasAttacked = 0;
	cherrier_2_team1.hasMoved = 0;

	// El�ve
	evan_1.idTeam = 1;
	strcpy(evan_1.typeUnit,"Evan");
	evan_1.idUnit = 23;
	evan_1.healthUnit = 100;
	evan_1.dexterityUnit = 4;
	evan_1.attackUnit = 10;
	evan_1.rangeUnit = 3;
	evan_1.posXUnit = 4; //4
	evan_1.posYUnit = 2; //2
	evan_1.hasAttacked = 0;
	evan_1.hasMoved = 0;

	evan_2.idTeam = 1;
	strcpy(evan_2.typeUnit,"Evan");
	evan_2.idUnit = 24;
	evan_2.healthUnit = 100;
	evan_2.dexterityUnit = 4;
	evan_2.attackUnit = 10;
	evan_2.rangeUnit = 3;
	evan_2.posXUnit = 4;
	evan_2.posYUnit = 5;
	evan_2.hasAttacked = 0;
	evan_2.hasMoved = 0;

	evan_3.idTeam = 1;
	strcpy(evan_3.typeUnit,"Evan");
	evan_3.idUnit = 25;
	evan_3.healthUnit = 100;
	evan_3.dexterityUnit = 4;
	evan_3.attackUnit = 10;
	evan_3.rangeUnit = 3;
	evan_3.posXUnit = 4;
	evan_3.posYUnit = 8;
	evan_3.hasAttacked = 0;
	evan_3.hasMoved = 0;

	/* Equipe rouge */
	// Sonia
	sonia_team2.idTeam = 2;
	strcpy(sonia_team2.typeUnit,"Sonia");
	sonia_team2.idUnit = 26;
	sonia_team2.healthUnit = 100;
	sonia_team2.dexterityUnit = 2;
	sonia_team2.attackUnit = 15;
	sonia_team2.rangeUnit = 4;
	sonia_team2.posXUnit = 15;
	sonia_team2.posYUnit = 5;
	sonia_team2.hasAttacked = 0;
	sonia_team2.hasMoved = 0;

	// Cherrier
	cherrier_1_team2.idTeam = 2;
	strcpy(cherrier_1_team2.typeUnit,"Cherrier");
	cherrier_1_team2.idUnit = 27;
	cherrier_1_team2.healthUnit = 100;
	cherrier_1_team2.dexterityUnit = 3;
	cherrier_1_team2.attackUnit = 20;
	cherrier_1_team2.rangeUnit = 2;
	cherrier_1_team2.posXUnit = 14;
	cherrier_1_team2.posYUnit = 3;
	cherrier_1_team2.hasAttacked = 0;
	cherrier_1_team2.hasMoved = 0;

	cherrier_2_team2.idTeam = 2;
	strcpy(cherrier_2_team2.typeUnit,"Cherrier");
	cherrier_2_team2.idUnit = 28;
	cherrier_2_team2.healthUnit = 100;
	cherrier_2_team2.dexterityUnit = 3;
	cherrier_2_team2.attackUnit = 20;
	cherrier_2_team2.rangeUnit = 2;
	cherrier_2_team2.posXUnit = 14;
	cherrier_2_team2.posYUnit = 7;
	cherrier_2_team2.hasAttacked = 0;
	cherrier_2_team2.hasMoved = 0;

	// El�ve
	zoe_1.idTeam = 2;
	strcpy(zoe_1.typeUnit,"Zo�");
	zoe_1.idUnit = 29;
	zoe_1.healthUnit = 100;
	zoe_1.dexterityUnit = 4;
	zoe_1.attackUnit = 10;
	zoe_1.rangeUnit = 3;
	zoe_1.posXUnit = 12;
	zoe_1.posYUnit = 2;
	zoe_1.hasAttacked = 0;
	zoe_1.hasMoved = 0;

	zoe_2.idTeam = 2;
	strcpy(zoe_2.typeUnit,"Zo�");
	zoe_2.idUnit = 30;
	zoe_2.healthUnit = 100;
	zoe_2.dexterityUnit = 4;
	zoe_2.attackUnit = 10;
	zoe_2.rangeUnit = 3;
	zoe_2.posXUnit = 12;
	zoe_2.posYUnit = 5;
	zoe_2.hasAttacked = 0;
	zoe_2.hasMoved = 0;

	zoe_3.idTeam = 2;
	strcpy(zoe_3.typeUnit,"Zo�");
	zoe_3.idUnit = 31;
	zoe_3.healthUnit = 100;
	zoe_3.dexterityUnit = 4;
	zoe_3.attackUnit = 10;
	zoe_3.rangeUnit = 3;
	zoe_3.posXUnit = 12;
	zoe_3.posYUnit = 8;
	zoe_3.hasAttacked = 0;
	zoe_3.hasMoved = 0;

	tabUnit[0] = sonia_team1;
    tabUnit[1] = cherrier_1_team1;
    tabUnit[2] = cherrier_2_team1;
    tabUnit[3] = evan_1;
    tabUnit[4] = evan_2;
    tabUnit[5] = evan_3;
    tabUnit[6] = sonia_team2;
    tabUnit[7] = cherrier_1_team2;
    tabUnit[8] = cherrier_2_team2;
    tabUnit[9] = zoe_1;
    tabUnit[10] = zoe_2;
    tabUnit[11] = zoe_3;
}

void reinitUnits(Unit tabUnit[NB_UNITS]){
    tabUnit[0].idTeam = 1;
	strcpy(tabUnit[0].typeUnit,"Sonia");
	tabUnit[0].idUnit = 20;
	tabUnit[0].healthUnit = 100;
	tabUnit[0].dexterityUnit = 2;
	tabUnit[0].attackUnit = 15;
	tabUnit[0].rangeUnit = 4;
	tabUnit[0].posXUnit = 1; //remettre � 1
	tabUnit[0].posYUnit = 5;
	tabUnit[0].hasAttacked = 0;
	tabUnit[0].hasMoved = 0;

	// Cherrier
	tabUnit[1].idTeam = 1;
	strcpy(tabUnit[1].typeUnit,"Cherrier");
	tabUnit[1].idUnit = 21;
	tabUnit[1].healthUnit = 100;
	tabUnit[1].dexterityUnit = 3;
	tabUnit[1].attackUnit = 20;
	tabUnit[1].rangeUnit = 2;
	tabUnit[1].posXUnit = 2;
	tabUnit[1].posYUnit = 3;
	tabUnit[1].hasAttacked = 0;
	tabUnit[1].hasMoved = 0;

	tabUnit[2].idTeam = 1;
	strcpy(tabUnit[2].typeUnit,"Cherrier");
	tabUnit[2].idUnit = 22;
	tabUnit[2].healthUnit = 100;
	tabUnit[2].dexterityUnit = 3;
	tabUnit[2].attackUnit = 20;
	tabUnit[2].rangeUnit = 2;
	tabUnit[2].posXUnit = 2;
	tabUnit[2].posYUnit = 7;
	tabUnit[2].hasAttacked = 0;
	tabUnit[2].hasMoved = 0;

	// El�ve
	tabUnit[3].idTeam = 1;
	strcpy(tabUnit[3].typeUnit,"Evan");
	tabUnit[3].idUnit = 23;
	tabUnit[3].healthUnit = 100;
	tabUnit[3].dexterityUnit = 4;
	tabUnit[3].attackUnit = 10;
	tabUnit[3].rangeUnit = 3;
	tabUnit[3].posXUnit = 4; //4
	tabUnit[3].posYUnit = 2; //2
	tabUnit[3].hasAttacked = 0;
	tabUnit[3].hasMoved = 0;

	tabUnit[4].idTeam = 1;
	strcpy(tabUnit[4].typeUnit,"Evan");
	tabUnit[4].idUnit = 24;
	tabUnit[4].healthUnit = 100;
	tabUnit[4].dexterityUnit = 4;
	tabUnit[4].attackUnit = 10;
	tabUnit[4].rangeUnit = 3;
	tabUnit[4].posXUnit = 4;
	tabUnit[4].posYUnit = 5;
	tabUnit[4].hasAttacked = 0;
	tabUnit[4].hasMoved = 0;

	tabUnit[5].idTeam = 1;
	strcpy(tabUnit[5].typeUnit,"Evan");
	tabUnit[5].idUnit = 25;
	tabUnit[5].healthUnit = 100;
	tabUnit[5].dexterityUnit = 4;
	tabUnit[5].attackUnit = 10;
	tabUnit[5].rangeUnit = 3;
	tabUnit[5].posXUnit = 4;
	tabUnit[5].posYUnit = 8;
	tabUnit[5].hasAttacked = 0;
	tabUnit[5].hasMoved = 0;

	/* Equipe rouge */
	// Sonia
	tabUnit[6].idTeam = 2;
	strcpy(tabUnit[6].typeUnit,"Sonia");
	tabUnit[6].idUnit = 26;
	tabUnit[6].healthUnit = 100;
	tabUnit[6].dexterityUnit = 2;
	tabUnit[6].attackUnit = 15;
	tabUnit[6].rangeUnit = 4;
	tabUnit[6].posXUnit = 15;
	tabUnit[6].posYUnit = 5;
	tabUnit[6].hasAttacked = 0;
	tabUnit[6].hasMoved = 0;

	// Cherrier
	tabUnit[7].idTeam = 2;
	strcpy(tabUnit[7].typeUnit,"Cherrier");
	tabUnit[7].idUnit = 27;
	tabUnit[7].healthUnit = 100;
	tabUnit[7].dexterityUnit = 3;
	tabUnit[7].attackUnit = 20;
	tabUnit[7].rangeUnit = 2;
	tabUnit[7].posXUnit = 14;
	tabUnit[7].posYUnit = 3;
	tabUnit[7].hasAttacked = 0;
	tabUnit[7].hasMoved = 0;

	tabUnit[8].idTeam = 2;
	strcpy(tabUnit[8].typeUnit,"Cherrier");
	tabUnit[8].idUnit = 28;
	tabUnit[8].healthUnit = 100;
	tabUnit[8].dexterityUnit = 3;
	tabUnit[8].attackUnit = 20;
	tabUnit[8].rangeUnit = 2;
	tabUnit[8].posXUnit = 14;
	tabUnit[8].posYUnit = 7;
	tabUnit[8].hasAttacked = 0;
	tabUnit[8].hasMoved = 0;

	// El�ve
	tabUnit[9].idTeam = 2;
	strcpy(tabUnit[9].typeUnit,"Zo�");
	tabUnit[9].idUnit = 29;
	tabUnit[9].healthUnit = 100;
	tabUnit[9].dexterityUnit = 4;
	tabUnit[9].attackUnit = 10;
	tabUnit[9].rangeUnit = 3;
	tabUnit[9].posXUnit = 12;
	tabUnit[9].posYUnit = 2;
	tabUnit[9].hasAttacked = 0;
	tabUnit[9].hasMoved = 0;

	tabUnit[10].idTeam = 2;
	strcpy(tabUnit[10].typeUnit,"Zo�");
	tabUnit[10].idUnit = 30;
	tabUnit[10].healthUnit = 100;
	tabUnit[10].dexterityUnit = 4;
	tabUnit[10].attackUnit = 10;
	tabUnit[10].rangeUnit = 3;
	tabUnit[10].posXUnit = 12;
	tabUnit[10].posYUnit = 5;
	tabUnit[10].hasAttacked = 0;
	tabUnit[10].hasMoved = 0;

	tabUnit[11].idTeam = 2;
	strcpy(tabUnit[11].typeUnit,"Zo�");
	tabUnit[11].idUnit = 31;
	tabUnit[11].healthUnit = 100;
	tabUnit[11].dexterityUnit = 4;
	tabUnit[11].attackUnit = 10;
	tabUnit[11].rangeUnit = 3;
	tabUnit[11].posXUnit = 12;
	tabUnit[11].posYUnit = 8;
	tabUnit[11].hasAttacked = 0;
	tabUnit[11].hasMoved = 0;
}


