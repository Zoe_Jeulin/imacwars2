#include "../include/const.h"
#include "../include/file.h"

void appendFile(File* to_visit, CaseMap* cell){
    FileNode* currentFile;
    FileNode* newNode = (FileNode*)malloc(sizeof(FileNode));
    newNode->cell = cell;
    newNode->next = NULL;

    if(to_visit->first==NULL){
        to_visit->first=newNode;
    }else{
        currentFile = to_visit->first;
        while(currentFile->next!=NULL){
            currentFile=currentFile->next;
        }
        currentFile->next=newNode;
    }
}


