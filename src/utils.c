#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "../include/const.h"
#include "../include/unit.h"
#include "../include/utils.h"

void createMap(int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW], Unit tabUnit[NB_UNITS]){
    int cptMenu = 0;

    for(int j=0; j<NB_CASES_HEIGHT_WINDOW; j++){
        for(int i=0; i<NB_CASES_WIDTH_WINDOW; i++){
            tabMap[i][j]=0;
        }
    }
    //Murs verticaux
    for(int i=1;i<NB_CASES_HEIGHT_WINDOW-1;i++){
        tabMap[0][i]=1; //Mur de gauche
        tabMap[16][i]=3; //Mur de droite
    }

    //Mur horizontal
    for(int i=1;i<NB_CASES_WIDTH_GAME;i++){
        tabMap[i][0]=2; //Mur du haut
    }

    //Angles
    tabMap[0][10]=4; //Angle bas gauche
    tabMap[16][10]=5; //Angle bas droit
    tabMap[0][0]=6; //Angle haut gauche
    tabMap[16][0]=7; //Angle haut droite

    //Fenetre gauche
    tabMap[2][0]=8; //Ext�rieur fenetre gauche
    tabMap[3][0]=10; //Milieu fenetre gauche
    tabMap[4][0]=10; //Milieu fenetre gauche
    tabMap[5][0]=10; //Milieu fenetre gauche
    tabMap[6][0]=11; //Int�rieur fenetre gauche

    //Fenetre droite
    tabMap[10][0]=12; //Interieur fenetre droite
    tabMap[11][0]=10; //Milieu fenetre droite
    tabMap[12][0]=10; //Milieu fenetre droite
    tabMap[13][0]=10; //Milieu fenetre droite
    tabMap[14][0]=9; //Exterieur fenetre droite

    //Tableau
    tabMap[16][2]=13; //Haut tableau
    tabMap[16][3]=14; //Milieu tableau
    tabMap[16][4]=14; //Milieu tableau
    tabMap[16][5]=14; //Milieu tableau
    tabMap[16][6]=14; //Milieu tableau
    tabMap[16][7]=14; //Milieu tableau
    tabMap[16][8]=15; //Bas tableau

    tabMap[0][2]=16; //Haut porte
    tabMap[0][3]=17; //Bas porte

    tabMap[8][0]=18; //Logo IMAC

    //Tables
    tabMap[3][1]=19;
    tabMap[14][1]=19;
    tabMap[8][2]=19;
    tabMap[14][2]=19;
    tabMap[8][3]=19;
    tabMap[5][4]=19;
    tabMap[8][4]=19;
    tabMap[9][4]=19;
    tabMap[10][7]=19;
    tabMap[6][8]=19;
    tabMap[7][8]=19;
    tabMap[10][8]=19;
    tabMap[2][9]=19;
    tabMap[6][9]=19;
    tabMap[2][10]=19;
    tabMap[6][10]=19;
    tabMap[13][10]=19;

    //Bonus
    tabMap[8][6]=32; //Feutre
    tabMap[8][1]=33; //Bi�re
    tabMap[8][8]=34; //Jeudimac

    //Affichage du menu
    for(int j=0;j<NB_CASES_HEIGHT_WINDOW;j++){
        for(int i=NB_CASES_WIDTH_GAME;i<NB_CASES_WIDTH_WINDOW;i++){
            tabMap[i][j]=35+cptMenu;
            cptMenu++;
        }
    }

    //Affichage unit�s
    for(int i=0 ; i<12 ; i++){
    	if(tabUnit[i].idUnit!=72){
    		tabMap[tabUnit[i].posXUnit][tabUnit[i].posYUnit]=tabUnit[i].idUnit;
    	}
    }
}

void getUnitClicked(Unit** unitSelected, Unit** enemySelected, int posClickX, int posClickY, int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW], Unit tabUnit[NB_UNITS], int textSelectCase){
    int i_index, j_index;
	i_index = posClickX/60;
	j_index = posClickY/60;
	if(tabMap[i_index][j_index]>=20 && tabMap[i_index][j_index]<=25){
        for(int i=0; i<NB_UNITS; i++){
            if(tabUnit[i].idUnit==tabMap[i_index][j_index]){
                *unitSelected = &tabUnit[i];
            }
        }
    }else if(tabMap[i_index][j_index]>=25 && tabMap[i_index][j_index]<=31){
        for(int i=0; i<NB_UNITS; i++){
            if(tabUnit[i].idUnit==tabMap[i_index][j_index]){
                *enemySelected = &tabUnit[i];
            }
        }
    }else{
        if(textSelectCase==0){
            *unitSelected = NULL;
        }
        *enemySelected = NULL;
    }
}

int aStar(int posXUnit, int posYUnit, int index_i, int index_j, int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW]){
    CaseMap tabCases[9][9]={0}; //tableau de structure CaseMap
    File to_visit; //file des cases � visiter, visit�es
    to_visit.first=NULL;
    CaseMap* current; //pointeur sur la case actuelle
    FileNode* currentFile;
    CaseMap* best;
    int cptDistance=0;
    //pour chaque CaseMap du tableau tabCases
    for(int j=0; j<9; j++){
        for(int i=0; i<9; i++){
            tabCases[i][j].x=i;
            tabCases[i][j].y=j;
            //si dans la case �quivalente de tabMap, il y a du sol/un bonus
            if(tabMap[posXUnit+i-4][posYUnit+j-4]==0 ||
            tabMap[posXUnit+i-4][posYUnit+j-4]==32 ||
            tabMap[posXUnit+i-4][posYUnit+j-4]==33 ||
            tabMap[posXUnit+i-4][posYUnit+j-4]==34){
                tabCases[i][j].cost=1;
            }else{
                tabCases[i][j].cost=999;
            }
            //enlever ou supprimer les index
            tabCases[i][j].heuristic=abs(index_i-(posXUnit-4)-i)+abs(index_j-(posYUnit-4)-j); //distance entre cette case et la case d'arriv�e (Manhattan)
            tabCases[i][j].visited=0;
            tabCases[i][j].next=NULL; //pas de parent au d�but
            tabCases[i][j].parent=NULL;
        }
    }

    current = &(tabCases[4][4]); //on commence par la case o� se trouve l'unit�

    while(current!=&tabCases[index_i-posXUnit+4][index_j-posYUnit+4] && current!=NULL){

        current->visited=1;

        if(current->x-1>=0 && tabCases[current->x-1][current->y].cost==1 && tabCases[current->x-1][current->y].visited==0){
            appendFile(&to_visit, &tabCases[current->x-1][current->y]);
            tabCases[current->x-1][current->y].parent=current;
        }

        if(current->x+1<9 && tabCases[current->x+1][current->y].cost==1 && tabCases[current->x+1][current->y].visited==0){
            appendFile(&to_visit, &tabCases[current->x+1][current->y]);
            tabCases[current->x+1][current->y].parent=current;
        }

        if(current->y-1>=0 && tabCases[current->x][current->y-1].cost==1 && tabCases[current->x][current->y-1].visited==0){
            appendFile(&to_visit, &tabCases[current->x][current->y-1]);
            tabCases[current->x][current->y-1].parent=current;
        }

        if(current->y+1<9 && tabCases[current->x][current->y+1].cost==1 && tabCases[current->x][current->y+1].visited==0){
            appendFile(&to_visit, &tabCases[current->x][current->y+1]);
            tabCases[current->x][current->y+1].parent=current;
        }

        if(to_visit.first!=NULL){
            currentFile = to_visit.first;
            if(currentFile->cell->visited==0){
                best=currentFile->cell;
            }else{
                best = NULL;
            }
            while(currentFile->next!=NULL){
                currentFile = currentFile->next;
                if(currentFile->cell->visited==0){
                    if(best==NULL){
                        best = currentFile->cell;
                    }else{
                        if(currentFile->cell->heuristic<best->heuristic){
                            best = currentFile->cell;
                        }
                    }
                }
            }
            current = best;
        }else{
            current = NULL;
        }
    }

    if(current!=NULL && current->parent!=NULL){
        while(current->parent!=NULL){
            cptDistance++;
            current=current->parent;
        }
        return cptDistance;
    }else{
        if(current!=NULL){
            return cptDistance;
        }else{
            return 99;
        }
    }
}

void loadText(char typeUnit[], char healthUnit[], char dexterityUnit[], char attackUnit[], char rangeUnit[],
                GLuint* texture_text_typeUnit, GLuint* texture_text_healthUnit, GLuint* texture_text_dexterityUnit, GLuint* texture_text_attackUnit, GLuint* texture_text_rangeUnit,
                SDL_Surface* tabSurfacesTexts[NB_TEXTS], TTF_Font* fontText){

    /* Chargement police */
    SDL_Color colorFontInfos = {67,33,1};

    tabSurfacesTexts[7] = TTF_RenderText_Blended(fontText, typeUnit, colorFontInfos); //type
    tabSurfacesTexts[8] = TTF_RenderText_Blended(fontText, healthUnit, colorFontInfos); //health
    tabSurfacesTexts[9] = TTF_RenderText_Blended(fontText, dexterityUnit, colorFontInfos); //dexterity
    tabSurfacesTexts[10] = TTF_RenderText_Blended(fontText, attackUnit, colorFontInfos); //attack
    tabSurfacesTexts[11] = TTF_RenderText_Blended(fontText, rangeUnit, colorFontInfos); //range

    glGenTextures(1, texture_text_typeUnit);
    glBindTexture(GL_TEXTURE_2D, *texture_text_typeUnit);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tabSurfacesTexts[7]->w, tabSurfacesTexts[7]->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, tabSurfacesTexts[7]->pixels);
    glBindTexture(GL_TEXTURE_2D, 0);

    glGenTextures(1, texture_text_healthUnit);
    glBindTexture(GL_TEXTURE_2D, *texture_text_healthUnit);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tabSurfacesTexts[8]->w, tabSurfacesTexts[8]->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, tabSurfacesTexts[8]->pixels);
    glBindTexture(GL_TEXTURE_2D, 0);

    glGenTextures(1, texture_text_dexterityUnit);
    glBindTexture(GL_TEXTURE_2D, *texture_text_dexterityUnit);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tabSurfacesTexts[9]->w, tabSurfacesTexts[9]->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, tabSurfacesTexts[9]->pixels);
    glBindTexture(GL_TEXTURE_2D, 0);

    glGenTextures(1, texture_text_attackUnit);
    glBindTexture(GL_TEXTURE_2D, *texture_text_attackUnit);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tabSurfacesTexts[10]->w, tabSurfacesTexts[10]->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, tabSurfacesTexts[10]->pixels);
    glBindTexture(GL_TEXTURE_2D, 0);

    glGenTextures(1, texture_text_rangeUnit);
    glBindTexture(GL_TEXTURE_2D, *texture_text_rangeUnit);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tabSurfacesTexts[11]->w, tabSurfacesTexts[11]->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, tabSurfacesTexts[11]->pixels);
    glBindTexture(GL_TEXTURE_2D, 0);

    for(int i=7; i<NB_TEXTS; i++){
        SDL_FreeSurface(tabSurfacesTexts[i]);
    }
}

int canPlay(int tabUnitsUsed[NB_UNITS/2]){
    for(int i=0; i<NB_UNITS/2; i++){
        if(tabUnitsUsed[i]==0){
            return 1;
        }
    }
    return 0;
}

int canAttack(Unit unit, int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW], Unit tabUnit[NB_UNITS], Unit* tabEnemiesAttackable[6]){
    int unitsToAttack = 0;
	int caseUnitEnemySonia, caseUnitEnemyCherrier1, caseUnitEnemyCherrier2,
	caseUnitEnemyStudent1, caseUnitEnemyStudent2, caseUnitEnemyStudent3;
	if(unit.idTeam == 1){
		caseUnitEnemySonia = 26;
		caseUnitEnemyCherrier1 = 27;
		caseUnitEnemyCherrier2 = 28;
		caseUnitEnemyStudent1 = 29;
		caseUnitEnemyStudent2 = 30;
		caseUnitEnemyStudent3 = 31;
	} else {
		caseUnitEnemySonia = 20;
		caseUnitEnemyCherrier1 = 21;
		caseUnitEnemyCherrier2 = 22;
		caseUnitEnemyStudent1 = 23;
		caseUnitEnemyStudent2 = 24;
		caseUnitEnemyStudent3 = 25;
	}
	for(int i = unit.posXUnit-unit.rangeUnit ; i <= unit.posXUnit+unit.rangeUnit ; i++){
		for(int j = unit.posYUnit-unit.rangeUnit ; j <= unit.posYUnit+unit.rangeUnit ; j++){
			if(abs(unit.posXUnit-i)+abs(unit.posYUnit-j) <= unit.rangeUnit){
				if(tabMap[i][j] == caseUnitEnemySonia ||
					tabMap[i][j] == caseUnitEnemyCherrier1 ||
					tabMap[i][j] == caseUnitEnemyCherrier2 ||
					tabMap[i][j] == caseUnitEnemyStudent1 ||
					tabMap[i][j] == caseUnitEnemyStudent2 ||
					tabMap[i][j] == caseUnitEnemyStudent3){
					tabEnemiesAttackable[unitsToAttack]=getUnitFromId(tabMap[i][j], tabUnit);
					unitsToAttack++;
                }
			}
		}
	}
	if(unitsToAttack > 0){
		return 1;
	} else {
		return 0;
	}
}

int canMove(Unit unit, int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW]){
    if((tabMap[unit.posXUnit][unit.posYUnit-1] == 0 || // case au dessus
		tabMap[unit.posXUnit][unit.posYUnit-1] == 32 ||
		tabMap[unit.posXUnit][unit.posYUnit-1] == 33 ||
		tabMap[unit.posXUnit][unit.posYUnit-1] == 34) ||
		(tabMap[unit.posXUnit][unit.posYUnit+1] == 0 || // case en dessous
		tabMap[unit.posXUnit][unit.posYUnit+1] == 32 ||
		tabMap[unit.posXUnit][unit.posYUnit+1] == 33 ||
		tabMap[unit.posXUnit][unit.posYUnit+1] == 34) ||
		(tabMap[unit.posXUnit-1][unit.posYUnit] == 0 || // case � gauche
		tabMap[unit.posXUnit-1][unit.posYUnit] == 32 ||
		tabMap[unit.posXUnit-1][unit.posYUnit] == 33 ||
		tabMap[unit.posXUnit-1][unit.posYUnit] == 34) ||
		(tabMap[unit.posXUnit+1][unit.posYUnit] == 0 || // case � droite
		tabMap[unit.posXUnit+1][unit.posYUnit] == 32 ||
		tabMap[unit.posXUnit+1][unit.posYUnit] == 33 ||
		tabMap[unit.posXUnit+1][unit.posYUnit] == 34)){
		return 1;
	} else {
		return 0;
	}
}

void attack(Unit* unitSelected, Unit* enemySelected, int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW]){
    enemySelected->healthUnit-=unitSelected->healthUnit*unitSelected->attackUnit/100;
    if(enemySelected->healthUnit>0){
        unitSelected->healthUnit-=enemySelected->healthUnit*enemySelected->attackUnit/100;
    }else{
        tabMap[enemySelected->posXUnit][enemySelected->posYUnit]=0;
    }

    if(unitSelected->healthUnit<=0){
        tabMap[unitSelected->posXUnit][unitSelected->posYUnit]=0;
    }
    unitSelected->hasAttacked=1;
}

int attackAI(Unit* unitSelected, Unit* tabEnemiesAttackable[6], int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW]){
    Unit* enemySelected = tabEnemiesAttackable[0];

    for(int i=0; i<NB_UNITS/2; i++){
        if(tabEnemiesAttackable[i]!=NULL && tabEnemiesAttackable[i]->healthUnit<enemySelected->healthUnit){
            enemySelected = tabEnemiesAttackable[i];
        }
    }
    attack(unitSelected, enemySelected, tabMap);

    return enemySelected->idUnit;
}

void move(Unit* unitSelected, int posClickX, int posClickY, int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW], int tabCasesAccessibles[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW]){
    int i_index, j_index;
	i_index = posClickX/60;
	j_index = posClickY/60;

	if(tabCasesAccessibles[i_index][j_index]==1){
        tabMap[unitSelected->posXUnit][unitSelected->posYUnit]=0;
        if(tabMap[i_index][j_index]==32 && unitSelected->idUnit==20){
            unitSelected->healthUnit+=20;
        }
        if(tabMap[i_index][j_index]==33 && (unitSelected->idUnit==21 || unitSelected->idUnit==22)){
            unitSelected->healthUnit+=20;
        }
        if(tabMap[i_index][j_index]==34 && (unitSelected->idUnit==23 || unitSelected->idUnit==24 || unitSelected->idUnit==25)){
            unitSelected->healthUnit+=20;
        }
        tabMap[i_index][j_index]=unitSelected->idUnit;
        unitSelected->posXUnit=i_index;
        unitSelected->posYUnit=j_index;
        unitSelected->hasMoved=1;
	}
}

void moveAI(Unit* unitSelected, int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW], Unit tabUnit[NB_UNITS]){
    int i=0;
    while(tabUnit[i].healthUnit<=0){
        i++;
    }
    Unit* unitTarget = &tabUnit[i];
    int xTargetCase = unitSelected->posXUnit;
    int yTargetCase = unitSelected->posYUnit;

    int posXBonus;
    int posYBonus;

    if(unitSelected->idUnit==26){
        for(int j=0; j<NB_CASES_HEIGHT_WINDOW; j++){
            for(int i=0; i<NB_CASES_WIDTH_WINDOW; i++){
                if(tabMap[i][j]==32){
                    posXBonus=i;
                    posYBonus=j;
                }
            }
        }
    }

    if(unitSelected->idUnit==27 || unitSelected->idUnit==28){
        for(int j=0; j<NB_CASES_HEIGHT_WINDOW; j++){
            for(int i=0; i<NB_CASES_WIDTH_WINDOW; i++){
                if(tabMap[i][j]==33){
                    posXBonus=i;
                    posYBonus=j;
                }
            }
        }
    }

    if(unitSelected->idUnit==29 || unitSelected->idUnit==30 || unitSelected->idUnit==31){
        for(int j=0; j<NB_CASES_HEIGHT_WINDOW; j++){
            for(int i=0; i<NB_CASES_WIDTH_WINDOW; i++){
                if(tabMap[i][j]==34){
                    posXBonus=i;
                    posYBonus=j;
                }
            }
        }
    }

    //s'il y a son bonus � port�e, on va en priorit� le chercher
    if(abs(unitSelected->posXUnit-posXBonus)+abs(unitSelected->posYUnit-posYBonus)<=unitSelected->dexterityUnit){
        tabMap[unitSelected->posXUnit][unitSelected->posYUnit]=0;
        unitSelected->healthUnit+=20;
        tabMap[posXBonus][posYBonus]=unitSelected->idUnit;
        unitSelected->posXUnit=posXBonus;
        unitSelected->posYUnit=posYBonus;
    }else{ //sinon on va vers l'unit� adverse la plus proche
        //on ne se d�place que si l'on est pas � c�t� d'une unit� ennemie (sinon pas besoin)
        if((tabMap[unitSelected->posXUnit-1][unitSelected->posYUnit]<20 || tabMap[unitSelected->posXUnit-1][unitSelected->posYUnit]>25) &&
       (tabMap[unitSelected->posXUnit+1][unitSelected->posYUnit]<20 || tabMap[unitSelected->posXUnit+1][unitSelected->posYUnit]>25) &&
       (tabMap[unitSelected->posXUnit][unitSelected->posYUnit-1]<20 || tabMap[unitSelected->posXUnit][unitSelected->posYUnit-1]>25) &&
       (tabMap[unitSelected->posXUnit][unitSelected->posYUnit+1]<20 || tabMap[unitSelected->posXUnit][unitSelected->posYUnit+1]>25)){
            for(int i=0; i<NB_UNITS/2; i++){
                if(abs(unitSelected->posXUnit-tabUnit[i].posXUnit)+abs(unitSelected->posYUnit-tabUnit[i].posYUnit)
                    < abs(unitSelected->posXUnit-unitTarget->posXUnit)+abs(unitSelected->posYUnit-unitTarget->posYUnit)
                   && tabUnit[i].healthUnit>0){
                    unitTarget = &tabUnit[i];
                }
            }

            aStarAI(unitSelected, unitTarget, tabMap, tabUnit, &xTargetCase, &yTargetCase);

            tabMap[unitSelected->posXUnit][unitSelected->posYUnit]=0;
            if(tabMap[xTargetCase][yTargetCase]==32 && unitSelected->idUnit==26){
                unitSelected->healthUnit+=20;
            }
            if(tabMap[xTargetCase][yTargetCase]==33 && (unitSelected->idUnit==27 || unitSelected->idUnit==28)){
                unitSelected->healthUnit+=20;
            }
            if(tabMap[xTargetCase][yTargetCase]==34 && (unitSelected->idUnit==29 || unitSelected->idUnit==30 || unitSelected->idUnit==31)){
                unitSelected->healthUnit+=20;
            }
            tabMap[xTargetCase][yTargetCase]=unitSelected->idUnit;
            unitSelected->posXUnit=xTargetCase;
            unitSelected->posYUnit=yTargetCase;
       }
    }
}

void aStarAI(Unit* unitSelected, Unit* unitTarget, int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW], Unit tabUnit[NB_UNITS], int* xTargetCase, int* yTargetCase){
    CaseMap tabCases[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW]={0}; //tableau de structure CaseMap
    File to_visit; //file des cases � visiter, visit�es
    to_visit.first=NULL;
    CaseMap* current; //pointeur sur la case actuelle
    FileNode* currentFile;
    CaseMap* best;
    //pour chaque CaseMap du tableau tabCases
    for(int j=0; j<NB_CASES_HEIGHT_WINDOW; j++){
        for(int i=0; i<NB_CASES_WIDTH_WINDOW; i++){
            tabCases[i][j].x=i;
            tabCases[i][j].y=j;
            //si dans la case �quivalente de tabMap, il y a du sol/un bonus
            if(tabMap[i][j]==0 || tabMap[i][j]==32 || tabMap[i][j]==33 || tabMap[i][j]==34 || tabMap[i][j]==unitTarget->idUnit){
                tabCases[i][j].cost=1;
            }else{
                tabCases[i][j].cost=999;
            }
            //enlever ou supprimer les index
            tabCases[i][j].heuristic=abs(i-unitTarget->posXUnit)+abs(j-unitTarget->posYUnit); //distance entre cette case et la case d'arriv�e (Manhattan)
            tabCases[i][j].visited=0;
            tabCases[i][j].next=NULL; //pas de parent au d�but
            tabCases[i][j].parent=NULL;
            tabCases[i][j].fils=NULL;
        }
    }

    current = &(tabCases[unitSelected->posXUnit][unitSelected->posYUnit]); //on commence par la case o� se trouve l'unit�

    while(current!=&tabCases[unitTarget->posXUnit][unitTarget->posYUnit] && current!=NULL){
        current->visited=1;

        if(current->x-1>=0 && tabCases[current->x-1][current->y].cost==1 && tabCases[current->x-1][current->y].visited==0){
            appendFile(&to_visit, &tabCases[current->x-1][current->y]);
            tabCases[current->x-1][current->y].parent=current;
        }

        if(current->x+1<NB_CASES_WIDTH_GAME && tabCases[current->x+1][current->y].cost==1 && tabCases[current->x+1][current->y].visited==0){
            appendFile(&to_visit, &tabCases[current->x+1][current->y]);
            tabCases[current->x+1][current->y].parent=current;
        }

        if(current->y-1>=0 && tabCases[current->x][current->y-1].cost==1 && tabCases[current->x][current->y-1].visited==0){
            appendFile(&to_visit, &tabCases[current->x][current->y-1]);
            tabCases[current->x][current->y-1].parent=current;
        }

        if(current->y+1<NB_CASES_HEIGHT_WINDOW && tabCases[current->x][current->y+1].cost==1 && tabCases[current->x][current->y+1].visited==0){
            appendFile(&to_visit, &tabCases[current->x][current->y+1]);
            tabCases[current->x][current->y+1].parent=current;
        }

        if(to_visit.first!=NULL){
            currentFile = to_visit.first;
            if(currentFile->cell->visited==0){
                best=currentFile->cell;
            }else{
                best = NULL;
            }
            while(currentFile->next!=NULL){
                currentFile = currentFile->next;
                if(currentFile->cell->visited==0){
                    if(best==NULL){
                        best = currentFile->cell;
                    }else{
                        if(currentFile->cell->heuristic<best->heuristic){
                            best = currentFile->cell;
                        }
                    }
                }
            }
            current->fils=best;
            current = best;
        }else{
            current = NULL;
        }
    }

    current=&(tabCases[unitSelected->posXUnit][unitSelected->posYUnit]);

    while(abs(current->x-unitTarget->posXUnit)+abs(current->y-unitTarget->posYUnit) > unitSelected->rangeUnit &&
          abs(current->x-unitSelected->posXUnit)+abs(current->y-unitSelected->posYUnit) <= unitSelected->dexterityUnit){
        current=current->fils;
    }

    *xTargetCase = current->x;
    *yTargetCase = current->y;
}

void clearTabUnitsUsed(int tabUnitsUsed[NB_UNITS/2], Unit tabUnit[NB_UNITS]){
    for(int i=0; i<NB_UNITS/2; i++){
        tabUnitsUsed[i]=0;
        tabUnit[i].hasAttacked=0;
        tabUnit[i].hasMoved=0;
    }
}

void clearTabEnemies(Unit* tabEnemiesAttackable[6]){
    for(int i=0; i<6; i++){
        tabEnemiesAttackable[i]=NULL;
    }
}

void clearTabCases(int tabCasesAccessibles[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW]){

    for(int j=0; j<NB_CASES_HEIGHT_WINDOW; j++){
        for(int i=0; i<NB_CASES_WIDTH_WINDOW; i++){
            tabCasesAccessibles[i][j]=0;
        }
    }
}

int checkGameOver(Unit tabUnit[NB_UNITS], int team){
    if(team==1){
        for(int i=NB_UNITS/2; i<NB_UNITS; i++){
            if(tabUnit[i].healthUnit>0){
                return 0;
            }
        }
        return 1;
    }else{
        for(int i=0; i<NB_UNITS/2; i++){
            if(tabUnit[i].healthUnit>0){
                return 0;
            }
        }
        return 2;
    }
}

void game(Unit tabUnit[NB_UNITS], int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW], GLuint tabTextures[NB_TEXTURES], GLuint tabTexturesTexts[NB_TEXTS],
           SDL_Surface* tabSurfaces[NB_SURFACES], SDL_Surface* tabSurfacesTexts[NB_TEXTS], TTF_Font* fontText){

    int screenStart = 0;
    int screenExplanation = 0;
    int gameOver = 0;
    int gameEnd = 0;
    int tour = 1;
    int tabUnitsUsed[6] = {0};
    Unit* tabEnemiesAttackable[6]={NULL};
    int tabCasesAccessibles[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW]={0};
    Unit* unitSelected = NULL;
    Unit* enemySelected = NULL;
    Uint32 timePressed = 0;
    Uint32 timeAttack = 0;
    Uint32 timeTour2 = 0;
    Uint32 timeAction = 0;
    int idUnitFlicker = -1;
    int idEnemyFlicker = -1;
    int buttonAttack = 0;
    int buttonMove = 0;
    int buttonEnd = 1;
    int buttonBack = 0;
    int buttonPressed = 0;
    int triangles = 0;
    int textSelectUnit = 1;
    int textSelectAction = 0;
    int textSelectEnemy = 0;
    int textSelectCase = 0;
    int cptActionsAI = 0;
    int indexAI = 6;

    while(!screenStart){
        SDL_Event e;
        Uint32 startTime = SDL_GetTicks();

        glClear(GL_COLOR_BUFFER_BIT);
        glEnable(GL_TEXTURE_2D);

        glBindTexture(GL_TEXTURE_2D, tabTextures[129]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(0,0);
            glTexCoord2f(0, 1);
            glVertex2d(0,WINDOW_HEIGHT);
            glTexCoord2f(1, 1);
            glVertex2d(WINDOW_WIDTH,WINDOW_HEIGHT);
            glTexCoord2f(1, 0);
            glVertex2d(WINDOW_WIDTH,0);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);

        drawButtonPlay(tabTextures, timePressed, startTime, buttonPressed);
        drawButtonQuit(tabTextures, timePressed, startTime, buttonPressed);

        glDisable(GL_TEXTURE_2D);
        SDL_GL_SwapBuffers();

        while(SDL_PollEvent(&e)){
            if(e.type == SDL_QUIT){
                screenStart = 1;
                screenExplanation = 1;
                gameOver = 2;
                gameEnd = 1;
                break;
            }
            if(e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE){
                screenStart = 1;
                screenExplanation = 1;
                gameOver = 2;
                gameEnd = 1;
                break;
            }
            if(e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_SPACE){
                screenStart = 1;
                break;
            }

            switch(e.type){
                case SDL_MOUSEBUTTONDOWN:
                    if(e.button.x >=457 && e.button.x<=642 && e.button.y>=545 && e.button.y<=584){
                        timePressed = SDL_GetTicks();
                        buttonPressed=5;
                        screenStart=1;
                    }
                    if(e.button.x >=682 && e.button.x<=867 && e.button.y>=545 && e.button.y<=584){
                        timePressed = SDL_GetTicks();
                        buttonPressed=6;
                        screenStart=1;
                        screenExplanation=1;
                        gameOver=2;
                    }
                    break;
                default:
                    break;
            }
        }
        /* Calcul du temps ecoule */
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        /* Si trop peu de temps s'est ecoule, on met en pause le programme */
        if(elapsedTime < FRAMERATE_MILLISECONDS){
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }
    }

    while(!screenExplanation){
        SDL_Event e;
        Uint32 startTime = SDL_GetTicks();

        glClear(GL_COLOR_BUFFER_BIT);
        glEnable(GL_TEXTURE_2D);

        glBindTexture(GL_TEXTURE_2D, tabTextures[130]);
        glPushMatrix();
        glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex2d(0,0);
            glTexCoord2f(0, 1);
            glVertex2d(0,WINDOW_HEIGHT);
            glTexCoord2f(1, 1);
            glVertex2d(WINDOW_WIDTH,WINDOW_HEIGHT);
            glTexCoord2f(1, 0);
            glVertex2d(WINDOW_WIDTH,0);
        glEnd();
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, 0);

        drawButtonStart(tabTextures, timePressed, startTime, buttonPressed);

        glDisable(GL_TEXTURE_2D);
        SDL_GL_SwapBuffers();
        while(SDL_PollEvent(&e)){
            if(e.type == SDL_QUIT){
                screenExplanation = 1;
                gameOver = 2;
                gameEnd = 1;
                break;
            }
            if(e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE){
                screenExplanation = 1;
                gameOver = 2;
                gameEnd = 1;
                break;
            }
            if(e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_SPACE){
                screenExplanation = 1;
                break;
            }

            switch(e.type){
                case SDL_MOUSEBUTTONDOWN:
                    if(e.button.x >= 1106 && e.button.x<=1289 && e.button.y>=592 && e.button.y<=631){
                        timePressed = SDL_GetTicks();
                        buttonPressed=7;
                        screenExplanation=1;
                    }
                    break;
                default:
                    break;
            }
        }
        /* Calcul du temps ecoule */
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        /* Si trop peu de temps s'est ecoule, on met en pause le programme */
        if(elapsedTime < FRAMERATE_MILLISECONDS){
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }
    }

    while(!gameOver){

        SDL_Event e;
        Uint32 startTime = SDL_GetTicks();

        glClear(GL_COLOR_BUFFER_BIT);
        glEnable(GL_TEXTURE_2D);
        drawMap(tabMap, tabTextures, startTime, idUnitFlicker, idEnemyFlicker); //modif
        if(startTime-timeAttack>DELAY_FLICKER*5){
            idUnitFlicker=-1;
            idEnemyFlicker=-1;
        }
        glDisable(GL_TEXTURE_2D);

        if(tour==1){
            glEnable(GL_TEXTURE_2D);
            drawPlayer(1,tabTextures);
            drawUnitsLeft(tabUnit,tabTextures);
            drawCursor(e.button.x, e.button.y, tabTextures, startTime);

            printInfos(e.button.x, e.button.y, tabUnit, tabTextures, tabTexturesTexts, tabSurfacesTexts, fontText);

            if(buttonAttack==1){
                drawButtonAttack(tabTextures, timePressed, startTime, buttonPressed);
            }
            if(buttonMove==1){
                drawButtonMove(tabTextures, timePressed, startTime, buttonPressed);
            }
            if(buttonEnd==1){
                drawButtonEnd(tabTextures, timePressed, startTime, buttonPressed);
            }
            if(buttonBack==1){
                drawButtonBack(tabTextures, timePressed, startTime, buttonPressed);
            }
            if(triangles==1 && unitSelected!=NULL){
                drawTriangles(unitSelected->posXUnit, unitSelected->posYUnit, tabTextures);
            }
            if(textSelectUnit==1){
                writeSelectUnit(tabTexturesTexts);
            }
            if(textSelectAction==1){
                writeSelectAction(tabTexturesTexts);
            }
            if(textSelectEnemy==1){
                writeSelectEnemy(tabTexturesTexts);
                if((int)floor(startTime-timePressed) >= 1000){
                    timePressed = SDL_GetTicks();
                }
                showEnemies(tabEnemiesAttackable, tabTextures, startTime, timePressed);
            }
            if(textSelectCase==1){
                writeSelectCase(tabTexturesTexts);
                if((int)floor(startTime-timePressed) >= 1000){
                    timePressed = SDL_GetTicks();
                }
                showCases(*unitSelected, tabMap, tabCasesAccessibles, tabTextures, startTime, timePressed);
            }
            glDisable(GL_TEXTURE_2D);
            if(canPlay(tabUnitsUsed)){

                /* Boucle traitant les evenements */
                while(SDL_PollEvent(&e)){

                    /* L'utilisateur ferme la fenetre : */
                    if(e.type == SDL_QUIT){
                        gameOver = 2;
                        break;
                    }

                    if(e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE){
                        gameOver = 2;
                        break;
                    }

                    /* Traitement d'evenements : */
                    switch(e.type){
                        /* Clic souris */
                        case SDL_MOUSEBUTTONDOWN:
                            if(buttonAttack==1 && e.button.x>=1080 && e.button.x<=1263 && e.button.y>=430 && e.button.y<=469){ //clic bouton attaque
                                timePressed = SDL_GetTicks();
                                buttonPressed=1;
                                buttonAttack=0;
                                buttonMove=0;
                                buttonEnd=0;
                                buttonBack=1;
                                textSelectUnit=0;
                                textSelectAction=0;
                                textSelectEnemy=1;
                                textSelectCase=0;
                            }else if(buttonMove==1 && e.button.x>=1080 && e.button.x<=1263 && e.button.y>=495 && e.button.y<=534){ //clic bouton se d�placer
                                timePressed = SDL_GetTicks();
                                buttonPressed=2;
                                buttonAttack=0;
                                buttonMove=0;
                                buttonEnd=0;
                                buttonBack=1;
                                textSelectUnit=0;
                                textSelectAction=0;
                                textSelectEnemy=0;
                                textSelectCase=1;
                            }else if(buttonEnd==1 && e.button.x>=1080 && e.button.x<=1263 && e.button.y>=560 && e.button.y<=599){ //clic bouton finir
                                timePressed = SDL_GetTicks();
                                buttonPressed=3;
                                buttonAttack=0;
                                buttonMove=0;
                                buttonEnd=0;
                                buttonBack=0;
                                textSelectUnit=0;
                                textSelectAction=0;
                                textSelectEnemy=0;
                                textSelectCase=0;
                                clearTabUnitsUsed(tabUnitsUsed, tabUnit);
                                tour=2;
                            }else if(buttonBack==1 && e.button.x>=1080 && e.button.x<=1263 && e.button.y>=560 && e.button.y<=599){ //clic bouton retour
                                timePressed = SDL_GetTicks();
                                buttonPressed=4;
                                if(canAttack(*unitSelected, tabMap, tabUnit, tabEnemiesAttackable)){
                                    buttonAttack=1;
                                }
                                if(canMove(*unitSelected, tabMap)){
                                    buttonMove=1;
                                }
                                buttonEnd=1;
                                buttonBack=0;
                                textSelectAction=1;
                                textSelectEnemy=0;
                                textSelectCase=0;
                            }else{
                                buttonPressed=0;
                                //voir si on a cliqu� sur une unit� (alli�e ou ennemie) et la r�cup�rer
                                getUnitClicked(&unitSelected, &enemySelected, e.button.x, e.button.y, tabMap, tabUnit, textSelectCase);
                                if(unitSelected!=NULL){ //si on a une unit� (alli�e) s�lectionn�e
                                    buttonAttack=0;
                                    buttonMove=0;
                                    buttonEnd=1;
                                    buttonBack=0;
                                    if(textSelectEnemy==0 || enemySelected==NULL){
                                        textSelectEnemy=0;
                                    }else{
                                        textSelectEnemy=1;
                                    }

                                    triangles=1;
                                    //si elle n'a pas d�j� attaqu� et qu'elle peut le faire
                                    if(!isUsed(*unitSelected,tabUnitsUsed) && unitSelected->hasAttacked==0 && canAttack(*unitSelected, tabMap, tabUnit, tabEnemiesAttackable)){
                                        buttonAttack=1;
                                        textSelectUnit=0;
                                        textSelectAction=1;
                                    }
                                    //si elle n'a pas d�j� boug� et qu'elle peut le faire
                                    if(!isUsed(*unitSelected,tabUnitsUsed) && unitSelected->hasMoved==0 && canMove(*unitSelected, tabMap)){
                                        buttonMove=1;
                                        textSelectUnit=0;
                                        textSelectAction=1;
                                    }
                                    //si on a cliqu� sur une unit� � nous puis sur une unit� ennemie
                                    //et qu'on attendait que le joueur s�lectionne une unit� � attaquer
                                    if(textSelectEnemy==1 && enemySelected!=NULL && unitSelected->hasAttacked==0){
                                        for(int i=0; i<6; i++){
                                            if(tabEnemiesAttackable[i]->idUnit==enemySelected->idUnit){ //si l'unit� ennemie s�lectionn�e est attaquable
                                                attack(unitSelected, enemySelected, tabMap);
                                                timePressed = SDL_GetTicks();
                                                gameOver=checkGameOver(tabUnit, 1);
                                                if(unitSelected->hasMoved==1 || !canMove(*unitSelected, tabMap)){
                                                    setUnitUsed(unitSelected->idUnit, tabUnitsUsed);
                                                }
                                                //modify
                                                timeAttack = SDL_GetTicks();
                                                idUnitFlicker=unitSelected->idUnit;
                                                idEnemyFlicker=enemySelected->idUnit;

                                                unitSelected=NULL;
                                                enemySelected=NULL;
                                                clearTabEnemies(tabEnemiesAttackable);
                                                triangles=0;
                                                buttonAttack=0;
                                                buttonMove=0;
                                                buttonEnd=1;
                                                buttonBack=0;
                                                textSelectUnit=1;
                                                textSelectAction=0;
                                                textSelectEnemy=0;
                                                textSelectCase=0;
                                                break;
                                            }else{
                                                textSelectEnemy=0;
                                            }
                                        }
                                    }
                                    if(textSelectCase==1 && unitSelected->hasMoved==0){
                                        move(unitSelected, e.button.x, e.button.y, tabMap, tabCasesAccessibles);

                                        if(unitSelected->hasMoved==1 && (unitSelected->hasAttacked==1 || !canAttack(*unitSelected, tabMap, tabUnit, tabEnemiesAttackable))){
                                            setUnitUsed(unitSelected->idUnit, tabUnitsUsed);
                                        }
                                        unitSelected=NULL;
                                        clearTabCases(tabCasesAccessibles);
                                        triangles=0;
                                        buttonAttack=0;
                                        buttonMove=0;
                                        buttonEnd=1;
                                        buttonBack=0;
                                        textSelectUnit=1;
                                        textSelectAction=0;
                                        textSelectEnemy=0;
                                        textSelectCase=0;
                                    }
                                }else{ //clic ni sur un bouton, ni sur une unit�
                                    triangles=0;
                                    buttonAttack=0;
                                    buttonMove=0;
                                    buttonEnd=1;
                                    buttonBack=0;
                                    textSelectUnit=1;
                                    textSelectAction=0;
                                    textSelectEnemy=0;
                                    textSelectCase=0;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }else{
                glEnable(GL_TEXTURE_2D);
                drawButtonEnd(tabTextures,timePressed,startTime, buttonPressed);
                glDisable(GL_TEXTURE_2D);
                triangles=0;
                buttonAttack=0;
                buttonMove=0;
                buttonEnd=1;
                buttonBack=0;
                textSelectUnit=0;
                textSelectAction=0;
                textSelectEnemy=0;
                textSelectCase=0;

                while(SDL_PollEvent(&e)){
                    if(e.type == SDL_QUIT){
                        gameOver = 1;
                        break;
                    }
                    if(e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE){
                        gameOver = 1;
                        break;
                    }

                    switch(e.type){
                        case SDL_MOUSEBUTTONDOWN:
                            if(e.button.x >= 1080 && e.button.x<=1263 && e.button.y>=550 && e.button.y<=589){
                                timePressed = SDL_GetTicks();
                                buttonPressed=3;
                                textSelectUnit=0;
                                textSelectAction=0;
                                textSelectEnemy=0;
                                textSelectCase=0;
                                clearTabUnitsUsed(tabUnitsUsed, tabUnit);
                                tour=2;
                            }else{
                                buttonPressed=0;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            SDL_GL_SwapBuffers();
        }

        if(tour==2){
            indexAI=6;
            cptActionsAI=0;
            timeTour2 = SDL_GetTicks();
            while(indexAI<NB_UNITS){
                SDL_Event e;
                Uint32 startTime = SDL_GetTicks();

                glEnable(GL_TEXTURE_2D);
                drawMap(tabMap, tabTextures, startTime, idUnitFlicker, idEnemyFlicker);
                drawPlayer(2,tabTextures);
                drawUnitsLeft(tabUnit,tabTextures);
                drawCursor(e.button.x, e.button.y, tabTextures, startTime);

                if(startTime-timeAttack>DELAY_FLICKER*5){
                    idUnitFlicker=-1;
                    idEnemyFlicker=-1;
                }
                printInfos(e.button.x, e.button.y, tabUnit, tabTextures, tabTexturesTexts, tabSurfacesTexts, fontText);
                glDisable(GL_TEXTURE_2D);

                timeAction=SDL_GetTicks();
                if(timeAction-timeTour2>=cptActionsAI*DELAY_ACTION_AI){
                    if(tabUnit[indexAI].hasAttacked==0 && isAlive(tabUnit[indexAI]) && canAttack(tabUnit[indexAI], tabMap, tabUnit, tabEnemiesAttackable)){
                        idEnemyFlicker=attackAI(&tabUnit[indexAI], tabEnemiesAttackable, tabMap);
                        cptActionsAI++;
                        timeAttack = SDL_GetTicks();
                        idUnitFlicker=tabUnit[indexAI].idUnit;
                        gameOver=checkGameOver(tabUnit,2);
                    }
                    timeAction=SDL_GetTicks();
                    if(timeAction-timeTour2>=cptActionsAI*DELAY_ACTION_AI){
                        if(tabUnit[indexAI].hasMoved==0 && isAlive(tabUnit[indexAI]) && canMove(tabUnit[indexAI], tabMap) && gameOver==0){
                            moveAI(&tabUnit[indexAI], tabMap, tabUnit);
                            tabUnit[indexAI].hasMoved=1;
                            cptActionsAI++;
                        }
                        if(tabUnit[indexAI].hasAttacked==1 && tabUnit[indexAI].hasMoved){
                            indexAI++;
                        }else{
                            timeAction=SDL_GetTicks();
                            if(timeAction-timeTour2>=cptActionsAI*DELAY_ACTION_AI){
                                if(tabUnit[indexAI].hasAttacked==0 && isAlive(tabUnit[indexAI]) && canAttack(tabUnit[indexAI], tabMap, tabUnit, tabEnemiesAttackable)){
                                    idEnemyFlicker=attackAI(&tabUnit[indexAI], tabEnemiesAttackable, tabMap);
                                    cptActionsAI++;
                                    timeAttack = SDL_GetTicks();
                                    idUnitFlicker=tabUnit[indexAI].idUnit;
                                    gameOver=checkGameOver(tabUnit,2);
                                }
                                indexAI++;
                            }
                        }
                    }

                }
                /* Boucle traitant les evenements */
                while(SDL_PollEvent(&e)){

                    /* L'utilisateur ferme la fenetre : */
                    if(e.type == SDL_QUIT){
                        gameOver = 1;
                        break;
                    }

                    if(e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE){
                        gameOver = 1;
                        break;
                    }
                }
                SDL_GL_SwapBuffers();
                clearTabEnemies(tabEnemiesAttackable);
            }
            for(int i=NB_UNITS/2; i<NB_UNITS; i++){
                tabUnit[i].hasAttacked=0;
                tabUnit[i].hasMoved=0;
            }

            buttonEnd=1;
            textSelectUnit=1;
            tour=1;
        }

        /* Calcul du temps ecoule */
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        /* Si trop peu de temps s'est ecoule, on met en pause le programme */
        if(elapsedTime < FRAMERATE_MILLISECONDS){
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }
    }

    while(!gameEnd){
        SDL_Event e;
        Uint32 startTime = SDL_GetTicks();

        glClear(GL_COLOR_BUFFER_BIT);
        glEnable(GL_TEXTURE_2D);
        if(gameOver==1){
            glBindTexture(GL_TEXTURE_2D, tabTextures[131]);
            glPushMatrix();
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2d(0,0);
                glTexCoord2f(0, 1);
                glVertex2d(0,WINDOW_HEIGHT);
                glTexCoord2f(1, 1);
                glVertex2d(WINDOW_WIDTH,WINDOW_HEIGHT);
                glTexCoord2f(1, 0);
                glVertex2d(WINDOW_WIDTH,0);
            glEnd();
            glPopMatrix();
            glBindTexture(GL_TEXTURE_2D, 0);
        }else{
            glBindTexture(GL_TEXTURE_2D, tabTextures[132]);
            glPushMatrix();
            glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex2d(0,0);
                glTexCoord2f(0, 1);
                glVertex2d(0,WINDOW_HEIGHT);
                glTexCoord2f(1, 1);
                glVertex2d(WINDOW_WIDTH,WINDOW_HEIGHT);
                glTexCoord2f(1, 0);
                glVertex2d(WINDOW_WIDTH,0);
            glEnd();
            glPopMatrix();
            glBindTexture(GL_TEXTURE_2D, 0);
        }

        drawButtonReplay(tabTextures, timePressed, startTime, buttonPressed);
        drawButtonQuit(tabTextures, timePressed, startTime, buttonPressed);

        glDisable(GL_TEXTURE_2D);
        SDL_GL_SwapBuffers();
        while(SDL_PollEvent(&e)){
            if(e.type == SDL_QUIT){
                gameEnd = 1;
                break;
            }
            if(e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE){
                gameEnd = 1;
                break;
            }

            switch(e.type){
                case SDL_MOUSEBUTTONDOWN:
                    if(e.button.x>=457 && e.button.x<=642 && e.button.y>=545 && e.button.y<=584){
                        timePressed = SDL_GetTicks();
                        buttonPressed=8;
                        gameEnd=1;
                        reinitUnits(tabUnit);
                        createMap(tabMap,tabUnit);
                        game(tabUnit, tabMap, tabTextures, tabTexturesTexts, tabSurfaces, tabSurfacesTexts, fontText);
                    }
                    if(e.button.x>=682 && e.button.x<=867 && e.button.y>=545 && e.button.y<=584){
                        timePressed = SDL_GetTicks();
                        buttonPressed=6;
                        gameEnd = 1;
                    }
                    break;
                default:
                    break;
            }
        }
        /* Calcul du temps ecoule */
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        /* Si trop peu de temps s'est ecoule, on met en pause le programme */
        if(elapsedTime < FRAMERATE_MILLISECONDS){
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }
    }
}

void reinitBonus(int tabMap[NB_CASES_WIDTH_WINDOW][NB_CASES_HEIGHT_WINDOW]){
    tabMap[8][6]=32; //Feutre
    tabMap[8][1]=33; //Bi�re
    tabMap[8][8]=34; //Jeudimac
}

void initTexts(SDL_Surface* tabSurfacesTexts[NB_TEXTS], GLuint tabTexturesTexts[NB_TEXTS], TTF_Font* fontText){
    SDL_Color colorFontInfos = {108,74,101};

    tabSurfacesTexts[0] = TTF_RenderText_Blended(fontText, "Clique sur l'une de tes", colorFontInfos);
    tabSurfacesTexts[1] = TTF_RenderText_Blended(fontText, "unit�s pour faire une action", colorFontInfos);

    tabSurfacesTexts[2] = TTF_RenderText_Blended(fontText, "Clique sur l'unit�", colorFontInfos);
    tabSurfacesTexts[3] = TTF_RenderText_Blended(fontText, "adverse � attaquer", colorFontInfos);

    tabSurfacesTexts[4] = TTF_RenderText_Blended(fontText, "Clique sur la case", colorFontInfos);
    tabSurfacesTexts[5] = TTF_RenderText_Blended(fontText, "o� tu veux te d�placer", colorFontInfos);

    tabSurfacesTexts[6] = TTF_RenderText_Blended(fontText, "Choisis une action", colorFontInfos);

    for(int i=0; i<7; i++){
        glGenTextures(1, &tabTexturesTexts[i]);
        glBindTexture(GL_TEXTURE_2D, tabTexturesTexts[i]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tabSurfacesTexts[i]->w, tabSurfacesTexts[i]->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, tabSurfacesTexts[i]->pixels);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

